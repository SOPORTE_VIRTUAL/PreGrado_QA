<?php

/**
 *      This file holds the configuration settings
 *      for phpmailer Class.
 *
 *      @package chamilo.configuration
 */

$platform_email['SMTP_FROM_EMAIL'] = (isset($administrator['email']) ? $administrator['email'] : 'admin@example.com');
$platform_email['SMTP_FROM_NAME'] = (isset($administrator['name']) ? $administrator['name'] : 'Admin');
$platform_email['SMTP_HOST']= '200.37.102.154';
$platform_email['SMTP_PORT']= 587;
$platform_email['SMTP_MAILER'] ='smtp'; // mail, sendmail or smtp
$platform_email['SMTP_AUTH'] = 0;
///$platform_email['SMTP_USER'] = 'Us1lsm7p';
//$platform_email['SMTP_PASS'] = 'rYd344hYT76io68';
$platform_email['SMTP_CHARSET'] = 'UTF-8';
//$platform_email['SMTP_UNIQUE_SENDER'] = 1; // to send all mails from the same user
//$platform_email['SMTP_DEBUG'] = 1; // change to 1 to enable smtp debug
//$platform_email['SMTP_SECURE'] = 'tls'; // if you're using SSL: ssl; or TLS: tls. (only used if SMTP_AUTH==1)
