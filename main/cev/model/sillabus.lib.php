<?php
//CEV CHANGE START GL
require_once '../../inc/lib/document.lib.php';
function enough_space($file_size, $max_dir_space) {
	if ($max_dir_space) {
		$already_filled_space = DocumentManager::documents_total_space();
		if (($file_size + $already_filled_space) > $max_dir_space) {
			return false;
		}
	}
	return true;
}
//CEV CHANGE END GL
/**
 * @author Jorge Frisancho Jibaja <jrfdeft@gmail.com>, USIL - Some changes to allow mandatory surveys
 * @version CEV CHANGE 139FEB2012
 */
function execute_sillabus_uploader($directory, $user_id){
    $size       = 0;
    $uploaded   = array();
    $total_file_uploaded = 0;

    if(substr($directory,-1) == '/'){
        $directory = substr($directory,0,-1);
    }
    if(!file_exists($directory) || !is_dir($directory) || !is_readable($directory)){
        Display::display_error_message(get_lang('DestDirectoryDoesntExist'));
        return -1;
    }
    if($handle = opendir($directory)){
        while(($file = readdir($handle)) !== false){
            $path = $directory.'/'.$file;
            $type = 'error';
            if($file != '.' && $file != '..'){
                if(is_file($path)){
                    $foo_size   = filesize($path);
                    $path_parts = pathinfo($path);
                    $type       = 'file';
                    $size       += $foo_size;
                    $uploaded[$file]['path'] = $path;
                    $uploaded[$file]['name'] = $path_parts['filename'];
                    $uploaded[$file]['ext']  = $path_parts['extension'];
                    $uploaded[$file]['size'] = $foo_size;
                    $uploaded[$file]['type'] = $type;
                    $uploaded[$file]['courses'] = seach_couses($uploaded[$file], $user_id);                      
                    //neat_r($uploaded[$file]);
                }
            }
        }
        closedir($handle);
    }
    return $uploaded;
}

/**
 * @author Jorge Frisancho Jibaja <jrfdeft@gmail.com>, USIL - Some changes to allow mandatory surveys
 * @version CEV CHANGE 139FEB2012
 */
function seach_couses($uploaded, $user_id){
    $course_table       = Database :: get_main_table(TABLE_MAIN_COURSE);
    $sys_course_path    = api_get_path(SYS_COURSE_PATH);
    $uploaded_file      = $uploaded;

    $uploaded_file['name']  = string_purificator($uploaded_file['name']);
    $uploaded_file['ext']   = string_purificator($uploaded_file['ext']);
    $uploaded_file['path']  = string_purificator($uploaded_file['path']);
    $uploaded_file['full']  = $uploaded_file['name'].'.'.$uploaded_file['ext'];

    Display::display_normal_message('Archivo: '.$uploaded_file['full'] , false);
    $dest_path      = '/';
    $prefix         = 'Silabo ';
        
    $sql = "SELECT code, directory, title, db_name 
            FROM $course_table
            WHERE title LIKE '".$uploaded_file['name']."%' 
            ORDER BY title";
    $result         = Database::query($sql);
    $num_row        = Database::num_rows($result);

    if ($num_row > 0){
        $courses = array ();
        while ($row = Database::fetch_row($result)){
            $uploaded_file['code']      = $row[0];
            $uploaded_file['dir']       = $row[1];
            $uploaded_file['title']     = $row[2];
            $uploaded_file['db_name']   = $row[3];
            $uploaded_file['dest_dir']  = $dest_path;
            $uploaded_file['prefix']    = $prefix;
            $uploaded_file['error']     = 0;
            $courses[$row[0]]           = $row;
            upload_sillabus($uploaded_file, $user_id);
        }
        return $courses;
    }
    else{
        Display::display_error_message(get_lang('TheListIsEmpty'));
        $uploaded_file['error']     = -1;
    }
}

/**
 * @author Jorge Frisancho Jibaja <jrfdeft@gmail.com>, USIL - Some changes to allow mandatory surveys
 * @version CEV CHANGE 139FEB2012
 */
function upload_sillabus($uploaded_file, $user_id, $output = true){
    // Variables
    $sys_course_path    = api_get_path(SYS_COURSE_PATH);
    $courseDir          = $uploaded_file['dir'].'/document';
    $base_work_dir      = $sys_course_path.$courseDir;
    $no_enough_space        = 0;
    $no_filter_extension    = 0;
    $no_is_dir              = 0;
    $file_exists            = 0;

    $is_ok  = true;

    $_course['sysCode'] = $uploaded_file['code'];
    $_course['dbName']  = $uploaded_file['db_name'];
    $max_filled_space = DocumentManager::get_course_quota();
    //echo '<br />max filled space = '.$max_filled_space;
    // Check if there is enough space to save the file
    if (!enough_space($uploaded_file['size'], $maxFilledSpace)) {
        $uploaded_file['error'] = Display::display_error_message('Codigo: '.$_course['sysCode'].' | '.$clean_name.' | '.get_lang('UplNotEnoughSpace'));
        $is_ok  = false;
        return $uploaded_file['error'];
    }

    $clean_name     = filename_cleaner($uploaded_file['prefix'].$uploaded_file['full']);
    if (!filter_extension($clean_name)) {
        $uploaded_file['error'] = Display::display_error_message('Codigo: '.$_course['sysCode'].' | '.$clean_name.' | '.get_lang('UplUnableToSaveFileFilteredExtension'));
        $is_ok  = false;
        return $uploaded_file['error'];
    }       
    else {
        // Extension is good
        //echo '<br />clean name = '.$clean_name;
        // If the upload path differs from / (= root) it will need a slash at the end
        $upload_path = $uploaded_file['dest_dir'];
        //echo '<br />upload_path = '.$upload_path;
        $file_path = $upload_path.$clean_name;
        //echo '<br />file path = '.$file_path;
        // Full path to where we want to store the file with trailing slash
        $where_to_save = $base_work_dir.$upload_path;
        // At least if the directory doesn't exist, tell so
        if (!is_dir($where_to_save)) {
            $uploaded_file['error'] = Display::display_error_message('Codigo: '.$_course['sysCode'].' | '.$clean_name.' | '.get_lang('DestDirectoryDoesntExist').' ('.$upload_path.')');
            $is_ok  = false;
            return $uploaded_file['error'];
        }
        //echo '<br />where to save = '.$where_to_save;
        // Full path of the destination
        $source_path    = utf8_decode($uploaded_file['path']);
        $store_path     = $where_to_save.$clean_name;
        //echo '<br />source path = '.$source_path;
        //echo '<br />store path = '.$store_path;
        // Name of the document without the extension (for the title)
        $document_name  = get_document_title($uploaded_file['prefix'].$uploaded_file['full']);
        //echo '<br />title = '.$document_name;
        // Size of the uploaded file (in bytes)
        $file_size = $uploaded_file['size'];

        $files_perm = api_get_permissions_for_new_files();
        if (file_exists($store_path)) {
            $is_ok  = false;
            $uploaded_file['error'] = Display::display_error_message('Codigo: '.$_course['sysCode'].' | '.$clean_name.' | '.get_lang('UplAlreadyExists'));
        }
        else {
            if (@copy($source_path, $store_path)) {
                chmod($store_path, $files_perm);

                // Put the document data in the database
                $document_id = add_document($_course, $file_path, 'file', $file_size, $document_name);
                if ($document_id) {
                    // Update document item_property
                    //echo '<br />Document ID: '.$document_id;
                    $is_ok  = api_item_property_update($_course, TOOL_DOCUMENT, $document_id, 'visible', $user_id, null, null, null, null, null);
                }

                // If the file is in a folder, we need to update all parent folders
                item_property_update_on_folder($_course,$upload_path,$user_id);
                // Display success message to user
                if ($is_ok && $output){
                    $uploaded_file['error'] = Display::display_confirmation_message('Codigo: '.$_course['sysCode'].' | '.$clean_name.' | '.get_lang('UplUploadSucceeded'), false);
                }
                return $file_path;
            }
            else {
                $uploaded_file['error'] = Display::display_error_message(get_lang('UplUnableToSaveFile'));
                return $uploaded_file['error'];
            }
        }
    }
}

/**
 * Adds a new document to the database
 *
 * @param array $_course
 * @param string $path
 * @param string $filetype
 * @param int $filesize
 * @param string $title
 * @return id if inserted document
 * @author Jorge Frisancho Jibaja <jrfdeft@gmail.com>, USIL - Some changes to allow mandatory surveys
 * @version CEV CHANGE 139FEB2012
 */
function insert_sillabus_to_db($course_db, $path, $filetype, $filesize, $title, $comment = null, $readonly = 0) {
        $session_id = api_get_session_id();
        $table_document = Database::get_course_table(TABLE_DOCUMENT, $course_db);
        $sql = "INSERT INTO $table_document
        (`path`, `filetype`, `size`, `title`, `comment`, `readonly`, `session_id`)
        VALUES ('$path','$filetype','$filesize','".
        Database::escape_string(htmlspecialchars($title, ENT_QUOTES, api_get_system_encoding()))."', '$comment', $readonly, $session_id)";
        if (Database::query($sql)) {
                //display_message("Added to database (id ".Database::insert_id().")!");
                return Database::insert_id();
        } else {
                //display_error("The uploaded file could not be added to the database (".Database::error().")!");
                return false;
        }
}

/*
 * Purified the name to copy the file to the document directory of the course.
 * @author Jorge Frisancho Jibaja <jrfdeft@gmail.com>, USIL - Some changes to allow mandatory surveys
 * @version CEV CHANGE 139FEB2012
 */
function filename_cleaner($file_name){
    // Strip slashes
    $file_name  = stripslashes($file_name);
    // Clean up the name, only ASCII characters should stay. (and strict)
    $file_name= replace_dangerous_char($file_name, 'strict');
    // No "dangerous" files
    $file_name = disable_dangerous_file($file_name);
    return $file_name;
}

/*
 * Is purified the file's name to make "the course search".
 * @author Jorge Frisancho Jibaja <jrfdeft@gmail.com>, USIL - Some changes to allow mandatory surveys
 * @version CEV CHANGE 139FEB2012
 */
function string_purificator($string){
    $string           = Database::escape_string(trim($string));
    $string           = utf8_encode($string);
    return $string;
}

/**
 * @author Jorge Frisancho Jibaja <jrfdeft@gmail.com>, USIL - Some changes to allow mandatory surveys
 * @version CEV CHANGE 139FEB2012
 */
function asdfas(){

    $upload_ok = process_uploaded_file($_FILES['user_upload']);
    if ($upload_ok) {
        // File got on the server without problems, now process it
        $new_path = handle_uploaded_document($_course, $_FILES['user_upload'], $base_work_dir, $_POST['curdirpath'], $_user['user_id'], null, null, null, null, 'overwrite');

        $new_comment = isset($_POST['comment']) ? trim($_POST['comment']) : '';
        $new_title = isset($_POST['title']) ? trim($_POST['title']) : '';

        if ($new_path && ($new_comment || $new_title)) {
            if (($docid = DocumentManager::get_document_id($_course, $new_path))) {
                $table_document = Database::get_course_table(TABLE_DOCUMENT);
                $ct = '';
                if ($new_comment) $ct .= ", comment='$new_comment'";
                if ($new_title)   $ct .= ", title='$new_title'";
                Database::query("UPDATE $table_document SET".substr($ct, 1)." WHERE id = '$docid'");
            }
        }
    }
}

/**
 * @author Jorge Frisancho Jibaja <jrfdeft@gmail.com>, USIL - Some changes to allow mandatory surveys
 * @version CEV CHANGE 139FEB2012
 */
function neat_r($arr, $return = false) {
    $out = array();
    $oldtab = "    ";
    $newtab = "  ";

    $lines = explode("\n", print_r($arr, true));

    foreach ($lines as $line) {

        //remove numeric indexes like "[0] =>" unless the value is an array
        if (substr($line, -5) != "Array") {    $line = preg_replace("/^(\s*)\[[0-9]+\] => /", "$1", $line, 1); }

        //garbage symbols
        foreach (array(
            "Array"        => "",
            "["            => "",
            "]"            => "",
            " =>"          => ":",
        ) as $old => $new) {
            $out = str_replace($old, $new, $out);
        }

        //garbage lines
        if (in_array(trim($line), array("Array", "(", ")", ""))) continue;

        //indents
        $indent = "";
        $indents = floor((substr_count($line, $oldtab) - 1) / 2);
        if ($indents > 0) { for ($i = 0; $i < $indents; $i++) { $indent .= $newtab; } }

        $out[] = $indent . trim($line);
    }

    $out = implode("<br />", $out) . "<br /><br /><br /><br />";
    if ($return == true) return $out;
    echo $out;
}

