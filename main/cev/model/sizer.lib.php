<?php
/**
 * @author Jorge Frisancho Jibaja <jrfdeft@gmail.com>, USIL - Some changes to allow mandatory surveys
 * @version CEV CHANGE 139FEB2012
 */
function filesize_r($path){
    if(!file_exists($path)) return 0;
    if(is_file($path)) return filesize($path);
    $ret = 0;
    foreach(glob($path."/*") as $fn)
    $ret += filesize_r($fn);
    return $ret;
}

/**
 * @author Jorge Frisancho Jibaja <jrfdeft@gmail.com>, USIL - Some changes to allow mandatory surveys
 * @version CEV CHANGE 139FEB2012
 */
function showSize($size_in_bytes) {
    $value = 0;
    if ($size_in_bytes >= 1073741824) {
        $value = round($size_in_bytes/1073741824*10)/10;
        return  ($round) ? round($value) . 'GB' : "{$value} GB";
    } else if ($size_in_bytes >= 1048576) {
        $value = round($size_in_bytes/1048576*10)/10;
        return  ($round) ? round($value) . 'MB' : "{$value} MB";
    } else if ($size_in_bytes >= 1024) {
        $value = round($size_in_bytes/1024*10)/10;
        return  ($round) ? round($value) . 'KB' : "{$value} KB";
    } else {
        return "{$size_in_bytes} Bytes";
    }
}

/**
 * @author Jorge Frisancho Jibaja <jrfdeft@gmail.com>, USIL - Some changes to allow mandatory surveys
 * @version CEV CHANGE 139FEB2012
 */
function recursive_directory_size($directory, $print = false){
    $size = 0;
    $foo  = array();

    if(substr($directory,-1) == '/'){
        $directory = substr($directory,0,-1);
    }
    if(!file_exists($directory) || !is_dir($directory) || !is_readable($directory)){
        return -1;
    }
    if($handle = opendir($directory)){
        while(($file = readdir($handle)) !== false){
            $path = $directory.'/'.$file;
            $type = 'error';
            if($file != '.' && $file != '..'){
                if(is_file($path)){
                    $foo_size   = filesize($path);
                    $size       += $foo_size;
                    $type       = 'file';
                }
                elseif(is_dir($path)){
                    $handlesize = recursive_directory_size($path);
                    if($handlesize >= 0){
                        $foo_size   = $handlesize;
                        $size       += $foo_size;
                        $type       = 'dir';
                    }
                    else{
                        return -1;
                    }
                }
                if ($print){
                    $foo[$file]['size']  = $foo_size;
                    $foo[$file]['type']  = $type;
                }
            }

        }
        closedir($handle);
    }
    if ($print){
            $foo['cevtotal'] = $size;
            return $foo;
    }
    else return $size;
}

/**
 * @author Jorge Frisancho Jibaja <jrfdeft@gmail.com>, USIL - Some changes to allow mandatory surveys
 * @version CEV CHANGE 139FEB2012
 */
function getFileList($dir, $print = false) {
    // array to hold return value
    $retval = array();
    // add trailing slash if missing
    if(substr($dir, -1) != "/") $dir .= "/";
    // open pointer to directory and read list of files
    $d = @dir($dir) or die("getFileList: Failed opening directory $dir for reading");
    while(false !== ($entry = $d->read())) {
        // skip hidden files if($entry[0] == ".") continue;
        if(is_dir("$dir$entry")){
            $retval[] = array( "name" => "$dir$entry/", "type" => filetype("$dir$entry"), "size" => 0, "lastmod" => filemtime("$dir$entry") );
        }
        elseif(is_readable("$dir$entry")) {
            $retval[] = array( "name" => "$dir$entry", "type" => mime_content_type("$dir$entry"), "size" => filesize("$dir$entry"), "lastmod" => filemtime("$dir$entry")  );
        }
    }
    $d->close();
    return $retval;
}

/**
 * @author Jorge Frisancho Jibaja <jrfdeft@gmail.com>, USIL - Some changes to allow mandatory surveys
 * @version CEV CHANGE 139FEB2012
 */
function dir_size( $dir ) {
    if( !$dir or !is_dir( $dir ) ) {
        return 0;
    }

    $ret = 0;
    $sub = opendir( $dir );

    while( $file = readdir( $sub ) ) {
        if( is_dir( $dir . '/' . $file ) && $file !== ".." && $file !== "." ) {
            $ret += dir_size( $dir . '/' . $file );
            unset( $file );
        }
        elseif( !is_dir( $dir . '/' . $file ) ) {
            $stats = stat( $dir . '/' . $file );
            $ret += $stats['size'];
            unset( $file );
        }
    }

    closedir( $sub );
    unset( $sub );
    return $ret;
}

/**
 * @author Jorge Frisancho Jibaja <jrfdeft@gmail.com>, USIL - Some changes to allow mandatory surveys
 * @version CEV CHANGE 139FEB2012
 */
function getArrayToHtml($directory_list,$useType = false){
    $class  = 'b';
    $result = '';
    $type   = 'dir';
    if (!empty($directory_list )){
        foreach ($directory_list as $key => $val) {
            if ($key != 'cevtotal'){
                if ($useType){
                    $type   = $val['type'];
                    $val    = $val['size'];
                }
                $result .= '<div class="'.$class.'"><img alt="'.$key.'" src="../css/'.$type.'.png"><strong>'.$key.'</strong> <em>'.showSize($val).'</em>'.date ("M d Y h:i:s A").'</div>';
                if($class=='b') $class='w';
                else $class = 'b';
            }
        }
    }
    return $result;
}
