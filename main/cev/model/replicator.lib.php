<?php
/**
 * @author Jorge Frisancho Jibaja <jrfdeft@gmail.com>, USIL - Some changes to allow mandatory surveys
 * @version CEV CHANGE 139FEB2012
 */

/**
 * Get the number of courses which will be displayed
 */
function get_number_of_courses() {
    $course_table = Database :: get_main_table(TABLE_MAIN_COURSE);
    $sql = "SELECT COUNT(code) AS total_number_of_items FROM $course_table";

    global $_configuration;

    if ((api_is_platform_admin() || api_is_session_admin()) && $_configuration['multiple_access_urls'] && api_get_current_access_url_id()!=-1) {
    	$access_url_rel_course_table = Database :: get_main_table(TABLE_MAIN_ACCESS_URL_REL_COURSE);
    	$sql.= " INNER JOIN $access_url_rel_course_table url_rel_course ON (code=url_rel_course.course_code)";
    }

    if (isset ($_REQUEST['keyword'])){
        $keyword = Database::escape_string($_REQUEST['keyword']);
        $sql .= " WHERE (title LIKE '%".$keyword."%' OR code LIKE '%".$keyword."%' OR visual_code LIKE '%".$keyword."%')";
    }
    elseif (isset ($_REQUEST['keyword_code'])){
        $keyword_code = Database::escape_string($_REQUEST['keyword_code']);
        $keyword_title = Database::escape_string($_REQUEST['keyword_title']);
        $keyword_category = Database::escape_string($_REQUEST['keyword_category']);
        $keyword_language = Database::escape_string($_REQUEST['keyword_language']);
        $keyword_visibility = Database::escape_string($_REQUEST['keyword_visibility']);
        $keyword_subscribe = Database::escape_string($_REQUEST['keyword_subscribe']);
        $keyword_unsubscribe = Database::escape_string($_REQUEST['keyword_unsubscribe']);
        $sql .= " WHERE (code LIKE '%".$keyword_code."%' OR visual_code LIKE '%".$keyword_code."%') AND title LIKE '%".$keyword_title."%' AND category_code LIKE '%".$keyword_category."%'  AND course_language LIKE '%".$keyword_language."%'   AND visibility LIKE '%".$keyword_visibility."%'    AND subscribe LIKE '%".$keyword_subscribe."%' AND unsubscribe LIKE '%".$keyword_unsubscribe."%'";
    }

     // adding the filter to see the user's only of the current access_url
    if ((api_is_platform_admin() || api_is_session_admin()) && $_configuration['multiple_access_urls'] && api_get_current_access_url_id()!=-1) {
        $sql.= " AND url_rel_course.access_url_id=".api_get_current_access_url_id();
    }

    $res = Database::query($sql);
    $obj = Database::fetch_object($res);
    return $obj->total_number_of_items;
}
/**
 * Get course data to display
 */
function get_course_data($from, $number_of_items, $column, $direction){
    $course_table = Database :: get_main_table(TABLE_MAIN_COURSE);
    $users_table = Database :: get_main_table(TABLE_MAIN_USER);
    $course_users_table = Database :: get_main_table(TABLE_MAIN_COURSE_USER);
    $sql = "SELECT code AS col0, visual_code AS col1, title AS col2, category_code AS col3, tutor_name as col4, code AS col5, visibility AS col6, directory as col7 FROM $course_table";
    //$sql = "SELECT code AS col0, visual_code AS col1, title AS col2, course_language AS col3, category_code AS col4, subscribe AS col5, unsubscribe AS col6, code AS col7, tutor_name as col8, code AS col9, visibility AS col10,directory as col11 FROM $course_table";
    global $_configuration;
    if ((api_is_platform_admin() || api_is_session_admin()) && $_configuration['multiple_access_urls'] && api_get_current_access_url_id()!=-1) {
        $access_url_rel_course_table = Database :: get_main_table(TABLE_MAIN_ACCESS_URL_REL_COURSE);
        $sql.= " INNER JOIN $access_url_rel_course_table url_rel_course ON (code=url_rel_course.course_code)";
    }

    if (isset ($_REQUEST['keyword'])){
        $keyword = Database::escape_string(trim($_REQUEST['keyword']));
        $sql .= " WHERE title LIKE '%".$keyword."%' OR code LIKE '%".$keyword."%' OR visual_code LIKE '%".$keyword."%'";
    }
    elseif (isset ($_REQUEST['keyword_code']))
    {
        $keyword_code = Database::escape_string($_REQUEST['keyword_code']);
        $keyword_title = Database::escape_string($_REQUEST['keyword_title']);
        $keyword_category = Database::escape_string($_REQUEST['keyword_category']);
        $keyword_language = Database::escape_string($_REQUEST['keyword_language']);
        $keyword_visibility = Database::escape_string($_REQUEST['keyword_visibility']);
        $keyword_subscribe = Database::escape_string($_REQUEST['keyword_subscribe']);
        $keyword_unsubscribe = Database::escape_string($_REQUEST['keyword_unsubscribe']);
        $sql .= " WHERE (code LIKE '%".$keyword_code."%' OR visual_code LIKE '%".$keyword_code."%') AND title LIKE '%".$keyword_title."%' AND category_code LIKE '%".$keyword_category."%'  AND course_language LIKE '%".$keyword_language."%'   AND visibility LIKE '%".$keyword_visibility."%' AND subscribe LIKE '%".$keyword_subscribe."%' AND unsubscribe LIKE '%".$keyword_unsubscribe."%'";
    }

     // adding the filter to see the user's only of the current access_url
    if ((api_is_platform_admin() || api_is_session_admin()) && $_configuration['multiple_access_urls'] && api_get_current_access_url_id()!=-1) {
        $sql.= " AND url_rel_course.access_url_id=".api_get_current_access_url_id();
    }

    $sql .= " ORDER BY col$column $direction ";
    $sql .= " LIMIT $from,$number_of_items";
    $res = Database::query($sql);
    $courses = array ();
    while ($course = Database::fetch_row($res))
    {
        //place colour icons in front of courses
        //$course[1] = '<nobr>'.get_course_visibility_icon($course[9]).'<a href="'.api_get_path(WEB_COURSE_PATH).$course[9].'/index.php">'.$course[1].'</a></nobr>';
        $course[1] = '<nobr>'.get_course_visibility_icon($course[6]).'<a href="'.api_get_path(WEB_COURSE_PATH).$course[5].'/index.php">'.$course[1].'</a></nobr>';
        //$course[7] = CourseManager :: is_virtual_course_from_system_code($course[7]) ? get_lang('Yes') : get_lang('No');
        //$course_rem = array($course[0],$course[1],$course[2],$course[3],$course[4],$course[5],$course[6],$course[7],$course[8],$course[9]);
        $course_rem = array($course[1],$course[2],$course[3],$course[4],$course[5]);
        $courses[] = $course_rem;
    }
    return $courses;
}
/**
 * Get course data to display to the next step
 */
function get_course_data2($from, $number_of_items, $column, $direction){
    $course_table = Database :: get_main_table(TABLE_MAIN_COURSE);
    $users_table = Database :: get_main_table(TABLE_MAIN_USER);
    $course_users_table = Database :: get_main_table(TABLE_MAIN_COURSE_USER);
    $sql = "SELECT code AS col0, visual_code AS col1, title AS col2, category_code AS col3, tutor_name as col4, code AS col5, visibility AS col6, directory as col7 FROM $course_table";
    //$sql = "SELECT code AS col0, visual_code AS col1, title AS col2, course_language AS col3, category_code AS col4, subscribe AS col5, unsubscribe AS col6, code AS col7, tutor_name as col8, code AS col9, visibility AS col10,directory as col11 FROM $course_table";
    global $_configuration;
    if ((api_is_platform_admin() || api_is_session_admin()) && $_configuration['multiple_access_urls'] && api_get_current_access_url_id()!=-1) {
        $access_url_rel_course_table = Database :: get_main_table(TABLE_MAIN_ACCESS_URL_REL_COURSE);
        $sql.= " INNER JOIN $access_url_rel_course_table url_rel_course ON (code=url_rel_course.course_code)";
    }

    $cidReq = Database::escape_string(trim($_REQUEST['cidReq']));

    if (isset ($_REQUEST['keyword'])){
        $keyword = Database::escape_string(trim($_REQUEST['keyword']));
        $sql .= " WHERE code NOT LIKE '".$cidReq."' AND (title LIKE '%".$keyword."%' OR code LIKE '%".$keyword."%' OR visual_code LIKE '%".$keyword."%')";
    }
    elseif (isset ($_REQUEST['keyword_code'])){
        $keyword_code = Database::escape_string($_REQUEST['keyword_code']);
        $keyword_title = Database::escape_string($_REQUEST['keyword_title']);
        $keyword_category = Database::escape_string($_REQUEST['keyword_category']);
        $keyword_language = Database::escape_string($_REQUEST['keyword_language']);
        $keyword_visibility = Database::escape_string($_REQUEST['keyword_visibility']);
        $keyword_subscribe = Database::escape_string($_REQUEST['keyword_subscribe']);
        $keyword_unsubscribe = Database::escape_string($_REQUEST['keyword_unsubscribe']);
        $sql .= " WHERE code NOT LIKE '".$cidReq."' AND (code LIKE '%".$keyword_code."%' OR visual_code LIKE '%".$keyword_code."%') AND title LIKE '%".$keyword_title."%' AND category_code LIKE '%".$keyword_category."%'  AND course_language LIKE '%".$keyword_language."%'   AND visibility LIKE '%".$keyword_visibility."%' AND subscribe LIKE '%".$keyword_subscribe."%' AND unsubscribe LIKE '%".$keyword_unsubscribe."%'";
    }

     // adding the filter to see the user's only of the current access_url
    if ((api_is_platform_admin() || api_is_session_admin()) && $_configuration['multiple_access_urls'] && api_get_current_access_url_id()!=-1) {
        $sql.= " AND url_rel_course.access_url_id=".api_get_current_access_url_id();
    }

    $sql .= " ORDER BY col$column $direction ";
    $sql .= " LIMIT $from,$number_of_items";
    $res = Database::query($sql);
    $courses = array ();
    while ($course = Database::fetch_row($res))
    {
        //place colour icons in front of courses
        //$course[1] = '<nobr>'.get_course_visibility_icon($course[9]).'<a href="'.api_get_path(WEB_COURSE_PATH).$course[9].'/index.php">'.$course[1].'</a></nobr>';
        $course[1] = '<nobr>'.get_course_visibility_icon($course[6]).'<a href="'.api_get_path(WEB_COURSE_PATH).$course[5].'/index.php">'.$course[1].'</a></nobr>';
        //$course[7] = CourseManager :: is_virtual_course_from_system_code($course[7]) ? get_lang('Yes') : get_lang('No');
        //$course_rem = array($course[0],$course[1],$course[2],$course[3],$course[4],$course[5],$course[6],$course[7],$course[8],$course[9]);
        $course_rem = array($course[0],$course[1],$course[2],$course[3],$course[4]);
        $courses[] = $course_rem;
    }
    return $courses;
}
/**
 * Filter to display the edit-buttons
 */
function modify_filter($code){
    global  $charset;
    return  '<a href="../controller/replicatorController.php?action=select_options&cidReq='.
            $code.'">'.
            Display::return_icon('backup.gif', 'Usar como Master').'</a>&nbsp;';
}
/**
 * Return an icon representing the visibility of the course
 */
function get_course_visibility_icon($v){
    $path = api_get_path(REL_CODE_PATH);
    $style = 'margin-bottom:-5px;margin-right:5px;';
    switch($v){
        case 0:
            return Display::return_icon('bullet_red.png', get_lang('CourseVisibilityClosed'), array('style'=>$style),22);
            break;
        case 1:
            return Display::return_icon('bullet_orange.png', get_lang('Private'), array('style'=>$style),22);
            break;
        case 2:
            return Display::return_icon('bullet_green.png', get_lang('OpenToThePlatform'), array('style'=>$style),22);
            break;
        case 3:
            return Display::return_icon('bullet_blue.png', get_lang('OpenToTheWorld'), array('style'=>$style),22);
            break;
        default:
            return '';
    }
}
?>
