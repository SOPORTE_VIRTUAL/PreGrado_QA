<?php
/**
 * This script add the mandatory attributead on course. survey table.
 * 
 * @author Jorge Frisancho Jibaja <jrfdeft@gmail.com>, USIL - Some changes to allow mandatory surveys
 * @version CEV CHANGE 13FEB2012
 */  

require_once dirname(__FILE__).'/../../inc/global.inc.php';

$this_section = SECTION_PLATFORM_ADMIN;
api_protect_admin_script();

$main_course_table 	= Database :: get_main_table(TABLE_MAIN_COURSE);
$course_list        = array ();
$start = 0;
$start = Security::remove_XSS($_REQUEST['start']);
if ($start == '1'){
    //Courses in which we suscribed out of any session
    $course_list_sql = "SELECT DISTINCT course.db_name as 'table'
                        FROM $main_course_table  course
                        WHERE visibility = 1   AND category_code like '5534%'";
    $course_list_sql_result = Database::query($course_list_sql);

    while ($row = Database::fetch_array($course_list_sql_result)){
        $table_survey = Database :: get_course_table(TABLE_SURVEY, $row['table']);
        $foo = '<div><ul>';
        $add_column_sql = "ALTER TABLE $table_survey
                          ADD COLUMN mandatory INT NOT NULL DEFAULT 0
                          AFTER avail_till";
        $add_column_sql_result = Database::query($add_column_sql);
        $result = $add_column_sql_result?'Ok':'Fail';
        $foo .= "<li>".$row['table']." $result.</li></ul></div>";
        echo $foo;
    }
}
else{
?>
    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Change show_score in Chamilo Data Base</title>
        <link media="all" type="text/css" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.17/themes/base/jquery-ui.css" rel="stylesheet">
        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.16/jquery-ui.min.js"></script>
        <script>
        $(function() {
            $( "input:submit" ).button();
            $( "input:submit" ).click(function() {
                $.post("addMandatoryColumn.php", { start: "1" },function(data){
                    $('.result').html(data);
                });
                $("input:submit").button( "option", "disabled", true );
            });
        });
        </script>
    </head>
    <body>
        <div class="demo">
            <input type="submit" value="Star the change!"/>
        </div>
        <div class="result"></div>
    </body>
    </html>
<?php
}

