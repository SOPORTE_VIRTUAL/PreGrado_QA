<?php
//@version CEV CHANGE 09FEB2012 cquispem@outlook.com compatibilidad a V1.10
$language_file = array('admin','courses');
$cidReset = true;
require_once dirname(__FILE__).'/../../inc/global.inc.php';

$this_section = SECTION_PLATFORM_ADMIN;
api_protect_admin_script();

require_once api_get_path(LIBRARY_PATH).'course.lib.php';
require_once api_get_path(LIBRARY_PATH).'formvalidator/FormValidator.class.php';
//CAMBIO DE sortabletable A NUEVA CLASE sortable_table para versión 1.10
require_once api_get_path(LIBRARY_PATH).'sortable_table.class.php';
require_once api_get_path(SYS_CODE_PATH).'cev/view/querySurveyView.php';

$action  = isset($_REQUEST['action'])?(empty($_REQUEST['action'])?'':$_REQUEST['action']):'';

switch ($action) {
	case 'query':
		global $_category;
		$_code = Security::remove_XSS($_REQUEST['keyword_code']);
		$_category = Security::remove_XSS($_REQUEST['keyword_category']);
		survey_list_view($_code, $_category);
		break;
	default:
		default_view();
		break;
}

function get_search_form($tool_name = 'Form'){
    // Get all course categories
    $table_course_category = Database :: get_main_table(TABLE_MAIN_CATEGORY);

    $form = new FormValidator('course_picker', 'post', 'querySurveyController.php?courses_per_page=1000');
    $form->addElement('header', '', $tool_name);
	$form->add_textfield('keyword_code', get_lang('SurveyCode'), false);
    $form->addElement('hidden', 'action', 'query');

    $categories = array();
    $categories_select = $form->addElement('select', 'keyword_category', get_lang('CourseFaculty'), $categories);
    $categories_select->addOption(get_lang('All'), '');
    CourseManager::select_and_sort_categories($categories_select);
    $form->addElement('style_submit_button', 'submit', get_lang('SearchCourse'),'class="search"');
    $form->display();
}

function survey_list($_code, $_category){
	$table_course 			= Database :: get_main_table(TABLE_MAIN_COURSE);

	//$sql = "SELECT c.code AS col0, c.title AS col1, s.code AS col2, s.survey_id AS col3 FROM $table_course c";
	$sql = "SELECT c.code FROM $table_course c";
	if (isset($_category) && !empty($_category))
	{
		$sql .= " WHERE c.category_code LIKE '%".$_category."%'";
	}
	$sql .= " LIMIT 0,3000";
	//echo $sql.'</br>';
	$res = Database::query($sql);
	$courses = array ();
	while ($course = Database::fetch_row($res))
	{
		$courses[] = $course[0];
	}
	
	$survey_list = array ();
	$count = 0;
	foreach ($courses as $index => $course_code){
		$course_info = api_get_course_info($course_code);
		$table_survey = Database :: get_course_table(TABLE_SURVEY, $course_info['dbName']);
		
		$sql = "SELECT c.code AS col0, c.title AS col1, s.code AS col2, s.survey_id AS col3 FROM $table_course c, $table_survey s WHERE c.code = '$course_code'";
		if (isset($_code) && !empty($_code))
		{
			$sql .= " AND s.code LIKE '%".$_code."%'";
		}
		$sql .= " LIMIT 0,3000";
		$res = Database::query($sql);
		//echo $sql.'</br>';	
		$count += mysql_num_rows($res);
		while ($survey = Database::fetch_row($res))
		{
			$survey_list[] = $survey;
		}
	}	
	
	$survey_list_ok    = (isset($survey_list) && !empty($survey_list) && is_array($survey_list) && (count($survey_list) > 0));
	$html = '';
	
    if ($survey_list_ok) {
		$html .= '<div class="actions" style="width:100%; text-align:center;">'.$count.'</div>';
		$html .= '<table class="data_table">
	<tbody><tr class="row_odd">
		<th>C�digo</th>
		<th>T�tulo</th>
		<th>C�digo Encuesta</th>
		<th>ID Encuesta</th>
	</tr>';
		$row_odd = false;
		
        foreach ($survey_list as $index => $row){
			if ($row_odd){
				$row_type = 'row_odd';
			}
			else{
				$row_type = 'row_even';
			}
			
			$html .= "<tr class=".'"'.$row_type.'"'.">
			<td>$row[0]</td>
			<td>$row[1]</td>
			<td>$row[2]</td>
			<td>$row[3]</td>
			</tr>";
        }
		$html .= '</tbody></table>';
		echo to_html($html);
    }
    else{
        Display::display_error_message('No exist any survey with this code: $_code.');
    }

	return $html;
}

function to_html($src){
$ent = array (
        "�" =>  "&aacute;",
        "�" =>  "&Aacute;",		
        "�" =>  "&aacute;",
        "�" =>  "&Eacute;",
		"�" =>  "&iacute;",
        "�" =>  "&Iacute;",		
        "�" =>  "&oacute;",
        "�" =>  "&Oacute;",
        "�" =>  "&uacute;",
        "�" =>  "&Uacute;",	
        "�" =>  "&Uuml;",
        "�" =>  "&uuml;",
		'�'=>'&Ntilde;', 
		'�'=>'&ntilde;'
    ); 

return strtr($src, $ent); 
}