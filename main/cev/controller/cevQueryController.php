<?php
/**
 * @author Jorge Frisancho Jibaja <jrfdeft@gmail.com>, USIL - Some changes to allow mandatory surveys
 * @version CEV CHANGE 09FEB2012
 */
require_once dirname(__FILE__).'/../../inc/global.inc.php';
require_once api_get_path(SYS_CODE_PATH).'cev/model/sizer.lib.php';

$this_section = SECTION_PLATFORM_ADMIN;
api_protect_admin_script();

$baseurl = $_SERVER['PHP_SELF'];

$action  = isset($_REQUEST['action'])?(empty($_REQUEST['action'])?'':$_REQUEST['action']):'';

switch ($action) {
    case 'name':
        if($_GET['order']=='desc') {
            krsort($directory_list);
            $fileurl = 'sort=name&amp;order=asc';
            $sizeurl = 'sort=size&amp;order=asc';
        }
        else $fileurl = 'sort=name&amp;order=desc';
        break;
    case 'upload':
        if($_GET['order']=='desc') {
            arsort($directory_list);
            $fileurl = 'sort=name&amp;order=asc';
            $sizeurl = 'sort=size&amp;order=asc';
        }
        else {
            asort($directory_list);
            $sizeurl = 'sort=size&amp;order=desc';
        }
        break;
    default:
        default_view();
        break;
}

function default_view(){
   $title = 'Seleccione su consulta: ';
   require_once '../../cev/view/cevQueryMenu.php';
}

function courseSizer_view(){
   require_once '../cev/view/cevlistaHistoria.php';
}
function tempSizer_view(){
   require_once '../cev/view/cevlistaPacientes.php';
}





