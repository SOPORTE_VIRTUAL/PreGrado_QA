<?php
require_once dirname(__FILE__).'/../../inc/global.inc.php';
require_once api_get_path(SYS_CODE_PATH).'cev/model/sizer.lib.php';

    
$main_course_table  = Database :: get_main_table(TABLE_MAIN_COURSE);
$course_list        = array();
$query              = array();

$this_is            = 'queryController';
$is_active_courses  = isset($_REQUEST['active'])?(empty($_REQUEST['active'])?'0':$_REQUEST['active']):'0';
$is_course_tmp      = isset($_REQUEST['tmp'])?(empty($_REQUEST['tmp'])?'0':$_REQUEST['tmp']):'0';
$order_by           = isset($_REQUEST['name'])?(empty($_REQUEST['name'])?'':$_REQUEST['name']):'';
$direction          = isset($_REQUEST['order'])?(empty($_REQUEST['order'])?'':$_REQUEST['order']):'';

$query['active']    = $is_active_courses;
$query['tmp']       = $is_course_tmp;
$query['txt']       = '<div class="b"><img alt="aaaa'.$is_course_active.$is_course_tmp.'" src="../css/folder.png"><strong>aaaa'.$is_course_active.$is_course_tmp.'/temp</strong> <em>'.showSize($is_course_active).$is_course_tmp.'</em>'.date ("M d Y h:i:s A").'</div>';

$title              = 'Tama&ntilde;o de los directorios ';
$condition = '';

if ($is_course_tmp){
        $title  .= ' "TEMP" ';
}

$title  .= 'de cada CURSO';

if ($is_active_courses == 1){
    $condition = 'WHERE visibility = 1';
    $title     .= ' "ACTIVO" ';
}

$course_list_sql        = "SELECT course.directory as 'Directory'
                FROM ".$main_course_table."  course $condition
                ORDER BY 1";

$course_list_sql_result = Database::query($course_list_sql);

while ($result_row      = Database::fetch_array($course_list_sql_result))
{
    $course_list[]      = $result_row['Directory'];
}

$directory_list         = array();
$count                  = 0;
$total                  = 0;

foreach ($course_list as $key => $value) {
    if ($is_course_tmp){
        $value           .= "/temp";
    }
    $path                   = api_get_path(SYS_COURSE_PATH).$value;
    $fullpath               = $path;

    $size                   = recursive_directory_size($fullpath);
    $key                    = strtoupper($value);
    $directory_list[$key]   = $size;
    $total += $size;
    $count ++;
}

$title  .= ' | Total Size: ' . showSize($total);

$fileurl    = 'asc';
$sizeurl    = 'asc';
//sort our files
switch ($order_by) {
    case 'name':
        if($direction == 'desc') {
            krsort($directory_list);
        }
        else $fileurl    = 'desc';
        break;
    case 'size':
        if($direction == 'desc') {
            arsort($directory_list);
        }
        else {
            asort($directory_list);
            $sizeurl    = 'desc';
        }
        break;
    default:
        $fileurl    = 'desc';
        $sizeurl    = 'desc';
        break;
}

$DOUBLEQUOTES = '"';
$SIMPLEQUOTES = "'";

$query['headerfile']    = '<a href="javascript:course_size_sort('.$is_active_courses.', '.$is_course_tmp.', '.$SIMPLEQUOTES.'name'.$SIMPLEQUOTES.', '.$SIMPLEQUOTES.$fileurl.$SIMPLEQUOTES.')">Directorio</a>';
$query['headersize']    = '<a href="javascript:course_size_sort('.$is_active_courses.', '.$is_course_tmp.', '.$SIMPLEQUOTES.'size'.$SIMPLEQUOTES.', '.$SIMPLEQUOTES.$sizeurl .$SIMPLEQUOTES.')">Tama&ntilde;o</a>';
$query['subtitle']      = $title;
$query['txt']           = getArrayToHtml($directory_list);
    
// For AJAX requests we'll return JSON object with current vote statistics
if($_SERVER['HTTP_X_REQUESTED_WITH']){
        header('Cache-Control: no-cache');
        $query = json_encode($query); // requires: PHP >= 5.2.0, PECL json >= 1.2.0
        echo   $query;
}
// For non-AJAX requests we are going to echo {$post_message} variable in main script
else
{
        $post_message = $query;
}

?>