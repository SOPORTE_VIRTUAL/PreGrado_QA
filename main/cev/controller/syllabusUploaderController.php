<?php

$language_file = array('document', 'course', 'admin');

/* Libraries */
require_once dirname(__FILE__).'/../../inc/global.inc.php';

$this_section = SECTION_PLATFORM_ADMIN;
api_protect_admin_script();

$lib_path = api_get_path(LIBRARY_PATH);
$code_path = api_get_path(SYS_CODE_PATH);
$sys_path = api_get_path(SYS_PATH);

require_once $lib_path.'usermanager.lib.php';
require_once $lib_path.'fileUpload.lib.php';
require_once $lib_path.'document.lib.php';
require_once dirname(__FILE__).'/../model/sillabus.lib.php';

$interbreadcrumb[]   = array ("url" => '../../admin/index.php', "name" => get_lang('PlatformAdmin'));
$tool_name          = get_lang('SillabusUploaderController');
Display :: display_header($tool_name);

$this_is            = 'sillabusUploaderController';
$query              = array();

$query['active']    = $is_active_courses;
$query['tmp']       = $is_course_tmp;

$directory_list     = array();

$path               = 'sillabus';
$fullpath           = $sys_path.$path;

$user_id            = $_user['user_id'];

if (!$user_id) {
    die('Not a valid user.');
}

$directory_list     = execute_sillabus_uploader($fullpath, $user_id);

if ($directory_list == -1) {
    echo 'Directorio invalido!';
}
else{
    //print_r($directory_list);
}
/* FOOTER */
Display :: display_footer();

/*$directory_list     = recursive_directory_size($fullpath, true);

$title              = 'Tama&ntilde;o de los directorios de la carpeta "UPLOAD/USERS" | Total Size: ' . showSize($directory_list['cevtotal']);

foreach ($course_list as $key => $value) {
    if ($is_course_tmp){
        $value           .= "/temp";
    }
    $path                   = api_get_path(SYS_COURSE_PATH).$value;
    $fullpath               = $path;

    $size                   = recursive_directory_size($fullpath);
    $key                    = strtoupper($value);
    $directory_list[$key]   = $size;
    $total += $size;
    $count ++;
}

$title  .= ' | Total Size: ' . showSize($total);

$fileurl    = 'asc';
$sizeurl    = 'asc';
$sTargetPath = ((isset($_POST['sourceDirectory']) && is_string($_POST['sourceDirectory']))? $_POST['sourceDirectory'] : '');

echo "The file ". $sTargetPath. " has been uploaded";*/

