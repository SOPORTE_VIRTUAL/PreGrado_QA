<?php
/**
 * @autor krodas kennyrodas@gmail.com
 * @update 20130409
 */
require_once dirname(__FILE__).'/../../inc/global.inc.php';

$this_section = SECTION_PLATFORM_ADMIN;
api_protect_admin_script();

$tool_name = 'Sync Password Moodle';
Display :: display_header($tool_name);
?>
<script>
function validar(){
	var resp = confirm("Realmente desea actualizar los passwords en Moodle?");
	return resp;
}
</script>

<form action="<?php echo $_SERVER['PHP_SELF'] ?>" method="post" onsubmit="return validar();">
<div class="row">
	<div class="form_header">Sync Password Moodle</div>
</div>

<div class="row">
	<div class="label">MOODLE</div>
	<div class="formw">
	<select name="platform">
		<option value="CPEL">CPEL</option>
		<option value="PREGRA">PRE-GRADO</option>
	</select></div>
</div>

<div class="row">
	<div class="label">Tipo dato</div>
	<div class="formw">
	<select name="tipo_dato">
		<!-- <option value="CURTIT">TITULO CURSO</option> -->
		<option value="CURCOD">COD CURSO</option>
		<!-- <option value="CATCOD">COD CATEGORIA</option> -->
	</select></div>
</div>

<div class="row">
	<div class="label">Dato</div>
	<div class="formw">
	<textarea rows="4" cols="25" name="dato"></textarea></div>
</div>

<div class="row">
	<div class="label">
	</div>
	<div class="formw"><button type="submit" name="submit" class="search">Actualizar Passwords</button>
	</div>
</div>

</form>

<?php


if (!isset($_POST['submit'])) exit();

$main_course_table = Database::get_main_table(TABLE_MAIN_COURSE);
$main_course_user_table = Database::get_main_table(TABLE_MAIN_COURSE_USER);
$main_course_user = Database::get_main_table(TABLE_MAIN_USER);

$tipo_dato = (isset($_POST['tipo_dato']))? Security::remove_XSS($_REQUEST['tipo_dato']) : false;
$dato = (isset($_POST['dato']))? Security::remove_XSS($_POST['dato']) : false;

$where = '';
// Datos ejemplo
//$caso = 'CURCOD'; $dato = "";
//$caso = 'CURTIT'; $dato = "";

// que moodle?
if ($_POST['platform'] == 'CPEL'){
	$moodle_db = 'evaluacionesdb.eva_user';
	$moodle_db_host = '172.16.128.19';
	$moodle_db_user = 'appmng';
	$moodle_db_pass = 'OuPP8C';
} elseif ($_POST['platform'] == 'PREGRA'){
	$moodle_db = 'moodle.mdl_user';
	$moodle_db_host = '172.16.128.140';
	$moodle_db_user = 'appmng';
	$moodle_db_pass = 'OuPP8C';
}

if ($tipo_dato == 'CURCOD'){
	$dato = str_replace(" ", "", $dato);
	$dato = "'".implode("','",explode(",", $dato))."'";
	$where .= ' AND c.code IN('.$dato.') ';
} elseif ($tipo_dato == 'CURTIT'){
	$where .= ' AND c.title LIKE "%'.$dato.'%" ';
} elseif ($tipo_dato == 'CATCOD'){	
	$where .= ' AND c.category_code = "'.$dato.'" ';
}

$querySelect = 'SELECT CONCAT("UPDATE '. $moodle_db .' SET PASSWORD = \'",u.password,"\' WHERE username =\'",u.username,"\';") as sqlupdate
	FROM '.$main_course_table.' c
	LEFT JOIN '.$main_course_user_table.' cu ON c.code = cu.course_code
	LEFT JOIN '.$main_course_user.' u ON u.user_id = cu.user_id
	WHERE 1=1 
	AND u.status = 5 ' 
	.$where.
	'GROUP BY u.user_id';

$result = Database::query($querySelect);
$aQueryUpdate = array();
//echo "<code>".$querySelect. " || ERROR: ". mysql_error()."</code>";

while ($row = Database::fetch_array($result)){
	array_push($aQueryUpdate, $row);
}

// echo "<pre>";
// print_r($aQueryUpdate);
// echo "</pre>";


$link = mysql_connect($moodle_db_host, $moodle_db_user, $moodle_db_pass);
if  (!$link) {
	die('No pudo conectarse a BD Moodle: ' . mysql_error());
}
/* $db_selected = mysql_select_db($dbName, $link);
if (!$db_selected) {
	die ('No se pudo seleccionar la BD Moodle : ' . mysql_error());
} */

$summary = '';
foreach ($aQueryUpdate as $sql) {
	mysql_query($sql['sqlupdate']);
	$summary .= "SQL: ".$sql['sqlupdate']." // Actualizados: ".mysql_affected_rows()." <br/>";
}
echo '<br/><br/>'.$summary;
mysql_close($link);

Display :: display_footer();