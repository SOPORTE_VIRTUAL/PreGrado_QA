<?php

//@version CEV CHANGE 04DEC2015 cquispem@outlook.com compatibilidad a V1.10
$language_file = array('course', 'coursebackup', 'admin');

$cidReset = true;
require_once '../../inc/global.inc.php';
$this_section = SECTION_PLATFORM_ADMIN;

api_protect_admin_script();
require_once api_get_path(LIBRARY_PATH) . 'course.lib.php';
require_once api_get_path(LIBRARY_PATH) . 'formvalidator/FormValidator.class.php';
//CAMBIO DE sortabletable A NUEVA CLASE sortable_table para versión 1.10
require_once api_get_path(LIBRARY_PATH) . 'sortable_table.class.php';
require_once api_get_path(SYS_CODE_PATH) . 'coursecopy/classes/CourseBuilder2.class.php';
require_once api_get_path(SYS_CODE_PATH) . 'coursecopy/classes/CourseRestorer.class.php';
require_once api_get_path(SYS_CODE_PATH) . 'coursecopy/classes/CourseSelectForm.class.php';

require_once '../../cev/model/replicator.lib.php';

$interbreadcrumb[] = array("url" => '../../admin/index.php', "name" => get_lang('PlatformAdmin'));
$tool_name = get_lang('CopyCourse');
Display :: display_header($tool_name);

$action = Security::remove_XSS($_REQUEST['action']);
$cidReq = Security::remove_XSS($_REQUEST['cidReq']);
// array must contain base course and destination course, 
//optionally, we can specify name of the destination course if this doesn't exists, type of copy, only aplicable to Linux based OS's
// and finally confirmation if course will be created, if name is not specified, it willbe created with name as the course code
$listCourses= array(
//'142144164000'=>array('142144'=>array('164000')),
 
//'142146164100'=>array('142146'=>array('164100'))  ,
    /*
'142668164200'=>array('142668','164200','','link',true),
'142768164300'=>array('142768'=>array('164300')),
'142066164400'=>array('142066'=>array('164400')),
'142441163946'=>array('142441'=>array('163946')),
'142591163947'=>array('142591'=>array('163947')),
'142144163948'=>array('142144'=>array('163948')),
'142146163949'=>array('142146'=>array('163949')),
'142668163950'=>array('142668'=>array('163950')),
'142768163951'=>array('142768'=>array('163951')),
'142066163952'=>array('142066'=>array('163952')),
'142441163953'=>array('142441'=>array('163953')),
'142591163954'=>array('142591'=>array('163954')),
'142144163955'=>array('142144'=>array('163955')),
'142146163956'=>array('142146'=>array('163956')),
'142668163957'=>array('142668'=>array('163957')),
'142768163958'=>array('142768'=>array('163958')),
'142066163959'=>array('142066'=>array('163959')),
'142441163960'=>array('142441'=>array('163960')),
'142591163961'=>array('142591'=>array('163961')),
'142144163962'=>array('142144'=>array('163962')),
'142146163963'=>array('142146'=>array('163963')),
'142668163964'=>array('142668'=>array('163964')),
'142768163965'=>array('142768'=>array('163965')),
'142066163966'=>array('142066'=>array('163966')),
'142441163967'=>array('142441'=>array('163967')),
'142591163968'=>array('142591'=>array('163968'))
    */
    

'142441163974'=>array('142441','999997','fundamentos de aaah6','0','0'),
'142591163975'=>array('142591','163975','','0','1'),
'142144163976'=>array('142144','163976','','0','1'),
'142146163977'=>array('142146','163977','','0','1'),
'142668163978'=>array('142668','163978','','0','1'),
'142768163979'=>array('142768','163979','','0','1'),
'142066163980'=>array('142066','163980','','0','1'),
'142441163981'=>array('142441','163981','','0','1'),
'142591163982'=>array('142591','163982','','0','1')


    
);

foreach ($listCourses as $item  ) {
print_r($item);
$orig=$item[0];
$dest=$item[1];
$courseName=  $item[2]==''?$dest:strtoupper($item[2]);
$copyAsSymlink= strtoupper(php_uname('s')) == 'LINUX'&&($item[3])=='1'?true:false;
$createCourseIfNotExists=($item[4])=='1'?true:false;
if ($orig!=null&&$dest!=null) {
    echo($orig);
    print_r($dest);
    echo('<br>');
    if (ValidateAndCreateCourse($dest,$courseName,$createCourseIfNotExists)) {
        replicator_course_to_course(true, $dest, null,  2,$orig);    
    }
   
}
else{
    echo('Error, no se encontró curso origen o destino en el array');
    echo('<br>');
    
}
   
}

/* FOOTER */
Display :: display_footer();

function ValidateAndCreateCourse($courseCode,$name=null,$createCourse=false) {

    $courseInfocheck = api_get_course_info($courseCode);
    if (empty($courseInfocheck)) {
        if (!$createCourse) {
            return false;
        }
        $msg = '' . api_get_path(WEB_COURSE_PATH) . ' '.$courseInfocheck['directory'] . ' ' . $courseInfocheck['title'] . ' ' . get_lang('Not') . " " . get_lang('Exists') . '<br />';
        Display::display_warning_message($msg);
                $course_language = 'spanish';
        $teacherList = array();
        $creatorId = api_get_user_id();

        $params = array();
        $params['title'] = $name==null?$courseCode:$name;
        $params['wanted_code'] =$courseCode;
        $params['tutor_name'] = null;
        $params['course_category'] = 'LANG';
        $params['course_language'] = 'spanish';
        $params['user_id'] = $creatorId;
        $params['add_user_as_teacher'] = false;

        $courseInfo = CourseManager::create_course($params);
        
             $msg = $courseInfo['directory'].' '.$courseInfo['title'].''.get_lang('Created').'<br />';
               Display::display_warning_message($msg);
              
    }
    else{
     $msg = '' . api_get_path(WEB_COURSE_PATH) . $courseInfocheck['directory'] . ' ' . $courseInfocheck['title'] . ' ' . get_lang('Already') . " " . get_lang('Exists') . '';
    Display::display_normal_message($msg);}
    return true;
}
function base64url_encode($plainText) {
    $base64 = base64_encode($plainText);
    $base64url = strtr($base64, '+/=', '-_,');
    return $base64url;
}

function base64url_decode($plainText) {
    $base64url = strtr($plainText, '-_,', '+/=');
    $base64 = base64_decode($base64url);
    return $base64;
}

function validate_resources($selected_resources) {
    if (array_key_exists(RESOURCE_LEARNPATH, $selected_resources) && !array_key_exists(RESOURCE_DOCUMENT, $selected_resources)) {
        $selected_resources[RESOURCE_DOCUMENT] = 1;
    }
}

function replicator_course_to_course($is_full, $course_code = null, $resources_selected = null, $same_file_name_option = 2,$course_orig=null){
    
    $courses_list_ok    = isset($course_code);
    if ($courses_list_ok) {
        $is_full = (isset($is_full) && !empty($is_full))?($is_full == '1')? true : false : false;
        $cb     = new CourseBuilder2($course_orig);
        print_r($cb);echo('<br>');
        echo('------------');
        $course = $cb->build(0,$course_orig);
        $cr = new CourseRestorer($course);
        $cr->set_file_option($same_file_name_option);
        $cr->restore($course_code);
        Display::display_normal_message(get_lang('CopyFinished').' '.$course_code);  
    }
    else{
        Display::display_error_message('No exist destination course.');
    }
}


function search_courses_destiantion($course_id, $hidden_fields = null, $is_full = true) {
    // Get all course categories
    $table_course_category = Database :: get_main_table(TABLE_MAIN_CATEGORY);
    $tool_name = get_lang('CopyCourse');

    $hidden_fields['is_full'] = $is_full;

    $form = new FormValidator('course_picker', 'post', 'replicatorController.php?cidReq=' . $course_id . '&courses_per_page=200');
    $form->addElement('header', '', $tool_name);


    // se modifica $form->add_textfield
    // por el método addText de la versión 110
    // $form->add_textfield('keyword_code', get_lang('CourseCode'), false);
    //$form->add_textfield('keyword_title', get_lang('Title'), false);

    $form->addText('keyword_code', get_lang('CourseCode'), true, false);
    $form->addText('keyword_title', get_lang('Title'), true, false);

    $form->addElement('hidden', 'option', 'select_courses');
    $form->addElement('hidden', 'action', 'select_courses');

    if (isset($hidden_fields) && !empty($hidden_fields) && is_array($hidden_fields)) {
        foreach ($hidden_fields as $key => $value) {
            $is_valid = ($key == 'selected_resources' && is_array($value));
            if (!$is_full && $is_valid) {
                $form->addElement('hidden', $key, base64_encode(serialize($value)));
            } else {
                $form->addElement('hidden', $key, $value);
            }
        }
    }

    $categories = array();
    $categories_select = $form->addElement('select', 'keyword_category', get_lang('CourseFaculty'), $categories);
    $categories_select->addOption(get_lang('All'), '');
    // se agregó el método select_and_sort_categories en la librería course.lib.php
    CourseManager::select_and_sort_categories($categories_select);
    // $form->addElement('style_submit_button', 'submit', get_lang('SearchCourse'),'class="search"');
    $form->addButtonSearch(get_lang('SearchCourse'), 'submit');
    $form->display();
}

function course_list($cidReq, $hidden_fields = null, $destination = false) {
    $parameters = array();
    if (isset($_REQUEST['keyword'])) {
        $parameters = array('keyword' => Security::remove_XSS($_REQUEST['keyword']));
    } elseif (isset($_REQUEST['keyword_code'])) {
        $parameters['keyword_code'] = Security::remove_XSS($_REQUEST['keyword_code']);
        $parameters['keyword_title'] = Security::remove_XSS($_REQUEST['keyword_title']);
        $parameters['keyword_category'] = Security::remove_XSS($_REQUEST['keyword_category']);
        $parameters['keyword_language'] = Security::remove_XSS($_REQUEST['keyword_language']);
        $parameters['keyword_visibility'] = Security::remove_XSS($_REQUEST['keyword_visibility']);
        $parameters['keyword_subscribe'] = Security::remove_XSS($_REQUEST['keyword_subscribe']);
        $parameters['keyword_unsubscribe'] = Security::remove_XSS($_REQUEST['keyword_unsubscribe']);
    }

    if (isset($cidReq)) {
        $parameters['cidReq'] = $cidReq;
    }

    if (isset($hidden_fields) && !empty($hidden_fields) && is_array($hidden_fields)) {
        foreach ($hidden_fields as $key => $value) {
            $parameters[$key] = $value;
        }
    }

    $tool_name = 'CEV - Course Replicator';

    // Create a search-box
    echo '<div class="actions">';
  echo'<form method="get" action="replicatorController.php" class="form-inline">
            <div class="form-group">
                <input class="form-control" type="text" name="keyword" value="" >
                <input type="hidden" value="search_setting" name="category">
                <button class="btn btn-default" type="submit">
                    <em class="fa fa-search"></em> ' . get_lang('Search') . '
                </button>
            </div>';
      if (isset($hidden_fields) && !empty($hidden_fields) && is_array($hidden_fields)) {
        foreach ($hidden_fields as $key => $value) {
          echo"  <input type='hidden' value='$value' name='$key'>";
        }
    }
  echo'</form>';
    echo '</div>';

    // Create a sortable table with the course data
    if (isset($destination) && $destination) {
        $table = new SortableTable('courses', 'get_number_of_courses', 'get_course_data2', 2);
    } else {
        $table = new SortableTable('courses', 'get_number_of_courses', 'get_course_data', 2);
    }

    $table->set_additional_parameters($parameters);
    if (isset($destination) && $destination == true) {
        $table->set_header(0, '', false, 'width="8px"');
        $table->set_header(1, get_lang('Code'));
        $table->set_header(2, get_lang('Title'));
        $table->set_header(3, get_lang('Category'));
        $table->set_header(4, get_lang('Teacher'));
        $table->set_form_actions(array('replicate_courses' => 'Replicar Curso'), 'destination_courses');
        $table->display();
    } else {
        //$table->set_header(0, '', false, 'width="8px"');
        $table->set_header(0, get_lang('Code'));
        $table->set_header(1, get_lang('Title'));
        $table->set_header(2, get_lang('Category'));
        $table->set_header(3, get_lang('Teacher'));
        $table->set_header(4, get_lang('Action'), false, 'width="145px"');
        $table->set_column_filter(4, 'modify_filter');
        $table->display();
    }
}

// modificación para generar symlinks en lugar de replicar curso completo
function select_replicate_option($cidReq) {
    $tool_name = get_lang('CopyCourse');

    $form = new FormValidator('select_resources', 'post', 'replicatorController.php?cidReq=' . $cidReq);
    $form->addElement('header', '', $tool_name);

    $form->addElement('radio', 'copy_option', '', get_lang('FullCopy'), 'full_copy');
    $form->addElement('radio', 'copy_option', '', get_lang('LetMeSelectItems'), 'select_items');
    // verificar que el S.O. se encuentre en alguna distribución de Linux
    if (strtoupper(php_uname('s')) == 'LINUX') {
         $form->addCheckBox(copyaslinks, '(Solo S.O. basados en Linux)', 'Copiar documentos como link referencial', '');
    }
    $form->addElement('hidden', 'action', 'select_resources');

    // create a radio button group
    $form->addElement('header', '', get_lang('SameFilename'));
    $form->addElement('radio', 'same_file_name_option', '', get_lang('SameFilenameSkip'), FILE_SKIP);
    $form->addElement('radio', 'same_file_name_option', '', get_lang('SameFilenameRename'), FILE_RENAME);
    $form->addElement('radio', 'same_file_name_option', '', get_lang('SameFilenameOverwrite'), FILE_OVERWRITE);

    //$form->addElement('style_submit_button', 'submit', get_lang('CopyCourse'),'class="save"');
    // se modifica $form->addElement('style_submit_button', 'submit', get_lang('CopyCourse'),'class="save"');
    // por el método addbuton save  de la versión 110
    $form->addButtonSave(get_lang('CopyCourse'), 'submit', false);

    $defaults['same_file_name_option'] = FILE_OVERWRITE;
    $defaults['copy_option'] = 'full_copy';
    $form->setDefaults($defaults);
    $form->display();
}

function select_replicate_resources($course, $cidReq, $hidden_fields = null, $avoid_serialize = false) {
    $resource_titles[RESOURCE_EVENT] = get_lang('Events');
    $resource_titles[RESOURCE_ANNOUNCEMENT] = get_lang('Announcements');
    $resource_titles[RESOURCE_DOCUMENT] = get_lang('Documents');
    $resource_titles[RESOURCE_LINK] = get_lang('Links');
    $resource_titles[RESOURCE_LEARNPATH] = get_lang('Learnpaths');
    $resource_titles[RESOURCE_SCORM] = 'SCORM';
    $resource_titles[RESOURCE_TOOL_INTRO] = get_lang('ToolIntro');
        $resource_titles[RESOURCE_COURSEDESCRIPTION] = get_lang('CourseDescription');
      
        
        
    $resource_titles[RESOURCE_FORUM] = get_lang('Forums');
    $resource_titles[RESOURCE_QUIZ] = get_lang('Tests');

    $resource_titles[RESOURCE_SURVEY] = get_lang('Survey');
    $resource_titles[RESOURCE_GLOSSARY] = get_lang('Glossary');
    $resource_titles[RESOURCE_WIKI] = get_lang('Wiki');
    $resource_titles[RESOURCE_ATTENDANCE] = get_lang('Attendances');
    $resource_titles[RESOURCE_TEST_CATEGORY] = get_lang('Category') . " " . get_lang('Tests');
    $resource_titles[RESOURCE_THEMATIC] = get_lang('Thematic');

    Display::display_normal_message(get_lang('ToExportLearnpathWithQuizYouHaveToSelectQuiz'));
    if (api_get_setting('show_glossary_in_documents') != 'none') {
        Display::display_normal_message(get_lang('ToExportDocumentsWithGlossaryYouHaveToSelectGlossary'));
    }

    $form = new FormValidator('select_resouces', 'post', 'replicatorController.php?cidReq=' . $cidReq);
    $form->addElement('header', '', get_lang('SelectResources'));

    $form->addElement('hidden', 'action', 'search_courses');
    $form->addElement('hidden', 'is_full', '0');

    if (isset($hidden_fields['same_file_name_option']) && !empty($hidden_fields['same_file_name_option'])) {
        $form->addElement('hidden', 'same_file_name_option', $hidden_fields['same_file_name_option']);
    } else {
        $form->addElement('hidden', 'same_file_name_option', 2);
    }

    $resources_list = array();
    // create a checkbox group
    foreach ($course->resources as $type => $resources) {
        if (count($resources) > 0) {
            switch ($type) {
                case RESOURCE_LINKCATEGORY:
                case RESOURCE_FORUMCATEGORY:
                case RESOURCE_FORUMPOST:
                case RESOURCE_FORUMTOPIC:
                case RESOURCE_QUIZQUESTION:
                case RESOURCE_SURVEYQUESTION:
                case RESOURCE_SURVEYINVITATION:
                case RESOURCE_SCORM:
                    break;
                default :
                    $obj_resources[] = &HTML_QuickForm::createElement('checkbox', $type, null, $resource_titles[$type]);
                /* foreach ($resources as $id => $resource) {
                  $resources_list[$type][]= $id;
                  } */
            }
        }
    }

    $form->addGroup($obj_resources, 'resources', 'Course Resources:', '<br />');

    if ($avoid_serialize) {
        //Documents are avoided due the huge amount of memory that the serialize php function "eats" (when there are directories with hundred/thousand of files)
        // this is a known issue of serialize
        $course->resources['document'] = null;
    }

    /* $form->addElement('hidden', 'resource', base64_encode(serialize($resources_list)));
      $form->addElement('hidden', 'course', base64_encode(serialize($course))); */

    // $form->addElement('style_submit_button', 'submit', get_lang('CopyCourse'),'class="save"');
    // se modifica $form->addElement('style_submit_button', 'submit', get_lang('CopyCourse'),'class="save"');
    // por el método addbuton save  de la versión 110
    $form->addButtonSave(get_lang('CopyCourse'), 'submit', false);
    $form->display();
}

/**
 * Get the posted course
 * @param string who calls the function? It can be copy_course, create_backup, import_backup or recycle_course
 * @return course The course-object with all resources selected by the user
 * in the form given by display_form(...)
 */
function get_master_course($from = '', $course_code = null, $course, $resources_selected) {
    //Create the resource DOCUMENT objects
    //Loading the results from the checkboxes of the javascript
    $resource = $resources_selected[RESOURCE_DOCUMENT];

    if (!empty($course_code)) {
        $course_info = api_get_course_info($course_code);
        $table_doc = Database :: get_course_table(TABLE_DOCUMENT, $course_info['dbName']);
        $table_prop = Database :: get_course_table(TABLE_ITEM_PROPERTY, $course_info['dbName']);
    } else {
        $table_doc = Database :: get_course_table(TABLE_DOCUMENT);
        $table_prop = Database :: get_course_table(TABLE_ITEM_PROPERTY);
    }

    // Searching the documents resource that have been set to null because $avoid_serialize is true in the display_form() function

    if ($from == 'copy_course') {
        if (is_array($resource)) {
            $resource = array_keys($resource);
            foreach ($resource as $resource_item) {
                $condition_session = '';
                if (!empty($session_id)) {
                    $session_id = intval($session_id);
                    $condition_session = ' AND d.session_id =' . $session_id;
                }

                $sql = 'SELECT d.id, d.path, d.comment, d.title, d.filetype, d.size  FROM ' . $table_doc . ' d, ' . $table_prop . ' p WHERE tool = \'' . TOOL_DOCUMENT . '\' AND p.ref = d.id AND p.visibility != 2 AND d.id = ' . $resource_item . $condition_session . ' ORDER BY path';
                $db_result = Database::query($sql);
                while ($obj = Database::fetch_object($db_result)) {
                    $doc = new Document($obj->id, $obj->path, $obj->comment, $obj->title, $obj->filetype, $obj->size);
                    $course->add_resource($doc);
                    // adding item property
                    $sql = "SELECT * FROM $table_prop WHERE TOOL = '" . RESOURCE_DOCUMENT . "' AND ref='" . $resource_item . "'";
                    $res = Database::query($sql);
                    $all_properties = array();
                    while ($item_property = Database::fetch_array($res, 'ASSOC')) {
                        $all_properties[] = $item_property;
                    }
                    $course->resources[RESOURCE_DOCUMENT][$resource_item]->item_properties = $all_properties;
                }
            }
        }
    }

    if (is_array($course->resources)) {
        foreach ($course->resources as $type => $resources) {
            switch ($type) {
                case RESOURCE_SURVEYQUESTION:
                    foreach ($resources as $id => $obj) {
                        if (is_array($resources_selected[RESOURCE_SURVEY]) && !in_array($obj->survey_id, array_keys($resources_selected[RESOURCE_SURVEY]))) {
                            unset($course->resources[$type][$id]);
                        }
                    }
                    break;
                case RESOURCE_LINKCATEGORY :
                case RESOURCE_FORUMCATEGORY :
                case RESOURCE_FORUMPOST :
                case RESOURCE_FORUMTOPIC :
                case RESOURCE_QUIZQUESTION :
                case RESOURCE_DOCUMENT:
                    // Mark folders to import which are not selected by the user to import,
                    // but in which a document was selected.
                    $documents = $resources_selected[RESOURCE_DOCUMENT];
                    if (is_array($resources))
                        foreach ($resources as $id => $obj) {
                            if ($obj->file_type == 'folder' && !isset($resources_selected[RESOURCE_DOCUMENT][$id]) && is_array($documents)) {
                                foreach ($documents as $id_to_check => $post_value) {
                                    $obj_to_check = $resources[$id_to_check];
                                    $shared_path_part = substr($obj_to_check->path, 0, strlen($obj->path));
                                    if ($id_to_check != $id && $obj->path == $shared_path_part) {
                                        $resources_selected[RESOURCE_DOCUMENT][$id] = 1;
                                        break;
                                    }
                                }
                            }
                        }
                default :
                    if (is_array($resources)) {
                        foreach ($resources as $id => $obj) {
                            $resource_is_used_elsewhere = $course->is_linked_resource($obj);
                            // check if document is in a quiz (audio/video)
                            if ($type == RESOURCE_DOCUMENT && $course->has_resources(RESOURCE_QUIZ)) {
                                foreach ($course->resources[RESOURCE_QUIZ] as $qid => $quiz) {
                                    if ($quiz->media == $id) {
                                        $resource_is_used_elsewhere = true;
                                    }
                                }
                            }
                            if (!isset($resources_selected[$type][$id]) && !$resource_is_used_elsewhere) {
                                unset($course->resources[$type][$id]);
                            }
                        }
                    }
            }
        }
    }
    return $course;
}

function get_resources($resources_list, $resources_selected) {
    $resources = array();
    foreach ($resources_selected as $type => $value) {
        if (count($resources_list[$type]) > 0) {
            foreach ($resources_list[$type] as $id => $resource) {
                $resources[$type][$id] = 'On';
            }
        }
    }
    return $resources;
}

?>
