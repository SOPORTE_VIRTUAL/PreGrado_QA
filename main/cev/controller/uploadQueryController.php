<?php
/**
 * @author Jorge Frisancho Jibaja <jrfdeft@gmail.com>, USIL - Some changes to allow mandatory surveys
 * @version CEV CHANGE 09FEB2012
 */
require_once dirname(__FILE__).'/../../inc/global.inc.php';
require_once api_get_path(SYS_CODE_PATH).'cev/model/sizer.lib.php';


$this_is            = 'uploadQueryController';
$order_by           = isset($_REQUEST['name'])?(empty($_REQUEST['name'])?'':$_REQUEST['name']):'';
$direction          = isset($_REQUEST['order'])?(empty($_REQUEST['order'])?'':$_REQUEST['order']):'';

$query              = array();

$query['active']    = $is_active_courses;
$query['tmp']       = $is_course_tmp;
    
$directory_list     = array();

$path               = 'main/upload/users';
$fullpath           = api_get_path(SYS_PATH).$path;

$directory_list     = recursive_directory_size($fullpath, true);

$title              = 'Tama&ntilde;o de los directorios de la carpeta "UPLOAD/USERS" | Total Size: ' . showSize($directory_list['cevtotal']);

$fileurl    = 'asc';
$sizeurl    = 'asc';
//sort our files
switch ($order_by) {
    case 'name':
        if($direction == 'desc') {
            krsort($directory_list);
        }
        else $fileurl    = 'desc';
        break;
    case 'size':
        if($direction == 'desc') {
            arsort($directory_list);
        }
        else {
            asort($directory_list);
            $sizeurl    = 'desc';
        }
        break;
    default:
        $fileurl    = 'desc';
        $sizeurl    = 'desc';
        break;
}

$DOUBLEQUOTES = '"';
$SIMPLEQUOTES = "'";

$query['headerfile']    = '<a href="javascript:upload_size_sort('.$SIMPLEQUOTES.'name'.$SIMPLEQUOTES.', '.$SIMPLEQUOTES.$fileurl.$SIMPLEQUOTES.')">Directorio</a>';
$query['headersize']    = '<a href="javascript:upload_size_sort('.$SIMPLEQUOTES.'size'.$SIMPLEQUOTES.', '.$SIMPLEQUOTES.$sizeurl .$SIMPLEQUOTES.')">Tama&ntilde;o</a>';
$query['subtitle']      = $title;
$query['txt']           = getArrayToHtml($directory_list, true);
    
// For AJAX requests we'll return JSON object with current vote statistics
if($_SERVER['HTTP_X_REQUESTED_WITH']){
        header('Cache-Control: no-cache');
        $query = json_encode($query); // requires: PHP >= 5.2.0, PECL json >= 1.2.0
        echo   $query;
}
// For non-AJAX requests we are going to echo {$post_message} variable in main script
else
{
        $post_message = $query;
}

