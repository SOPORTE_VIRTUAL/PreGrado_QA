<?php
# GLOBAL VARIABLES
$url = "../cev/8859-9.TXT";
//$url = "8859-9.txt";
$iso2utf = array();
$utf2iso = array();

# UNICODE MAPPING TABLE PARSING
function create_map($url){
    global $iso2utf, $utf2iso;
    $fl = @(file($url)) OR (die("cannot open file : $url\n"));
    for ($i=0; $i<count($fl); $i++){
        if($fl[$i][0] != '#' && trim($fl[$i])){
            list($iso, $uni, $s, $desc) = split("\t",$fl[$i]);
            $iso2utf[$iso] = $uni;
            $utf2iso[$uni] = $iso;
        }
    }
}

# FINDING UNICODE LETTER'S DECIMAL ASCII VALUE
function uniord($c){
    $ud = 0;
    if (ord($c{0})>=0 && ord($c{0})<=127)   $ud = $c{0};
    if (ord($c{0})>=192 && ord($c{0})<=223) $ud = (ord($c{0})-192)*64 + (ord($c{1})-128);
    if (ord($c{0})>=224 && ord($c{0})<=239) $ud = (ord($c{0})-224)*4096 + (ord($c{1})-128)*64 + (ord($c{2})-128);
    if (ord($c{0})>=240 && ord($c{0})<=247) $ud = (ord($c{0})-240)*262144 + (ord($c{1})-128)*4096 + (ord($c{2})-128)*64 + (ord($c{3})-128);
    if (ord($c{0})>=248 && ord($c{0})<=251) $ud = (ord($c{0})-248)*16777216 + (ord($c{1})-128)*262144 + (ord($c{2})-128)*4096 + (ord($c{3})-128)*64 + (ord($c{4})-128);
    if (ord($c{0})>=252 && ord($c{0})<=253) $ud = (ord($c{0})-252)*1073741824 + (ord($c{1})-128)*16777216 + (ord($c{2})-128)*262144 + (ord($c{3})-128)*4096 + (ord($c{4})-128)*64 + (ord($c{5})-128);
    if (ord($c{0})>=254 && ord($c{0})<=255) $ud = false; //error
    return $ud;
}

# PARSING UNICODE STRING
function utf2iso($source) {
    global $utf2iso;
    $pos = 0;
    $len = strlen ($source);
    $encodedString = '';

    while ($pos < $len) {
        $is_ascii = false;
        $asciiPos = ord (substr ($source, $pos, 1));
        if(($asciiPos >= 240) && ($asciiPos <= 255)) {
            // 4 chars representing one unicode character
            $thisLetter = substr ($source, $pos, 4);
            $thisLetterOrd = uniord($thisLetter);
            $pos += 4;
        }
        else if(($asciiPos >= 224) && ($asciiPos <= 239)) {
            // 3 chars representing one unicode character
            $thisLetter = substr ($source, $pos, 3);
            $thisLetterOrd = uniord($thisLetter);
            $pos += 3;
        }
        else if(($asciiPos >= 192) && ($asciiPos <= 223)) {
            // 2 chars representing one unicode character
            $thisLetter = substr ($source, $pos, 2);
            $thisLetterOrd = uniord($thisLetter);
            $pos += 2;
        }
        else{
            // 1 char (lower ascii)
            $thisLetter = substr ($source, $pos, 1);
            $thisLetterOrd = uniord($thisLetter);
            $pos += 1;
            $encodedString .= $thisLetterOrd;
            $is_ascii = true;
        }
        if(!$is_ascii){
            $hex = sprintf("%X", $thisLetterOrd);
            if(strlen($hex)<4) for($t=strlen($hex);$t<4;$t++)$hex = "0".$hex;
            $hex = "0x".$hex;
            $hex = $utf2iso[$hex];
            $hex = str_replace('0x','',$hex);
            $dec = hexdec($hex);
            $encodedString .= sprintf("%c", $dec);
        }
    }
    return $encodedString;
}

# CREATING ISO2UTF & UTF2ISO MAPS
create_map($url);
?>
