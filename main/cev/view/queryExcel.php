<?php
/**
 * @author Jorge Frisancho Jibaja <jrfdeft@gmail.com>, USIL - Some changes to allow mandatory surveys
 * @version CEV CHANGE 09FEB2012
 */
require_once dirname(__FILE__).'/../../inc/global.inc.php';

$interbreadcrumb[]  = array ("url" => '../../admin/index.php', "name" => get_lang('PlatformAdmin'));
$tool_name          = 'QueryToExcel';

$user_id	= Security::remove_XSS($_REQUEST['user_id']);
$cidReq		= Security::remove_XSS($_REQUEST['cidReq']);
$last_date	= Security::remove_XSS($_REQUEST['last_date']);
$query_limit	= Security::remove_XSS($_REQUEST['query_limit']);

//Display :: display_header($tool_name);

$tbl_track_course 	= Database :: get_statistic_table(TABLE_STATISTIC_TRACK_E_COURSE_ACCESS);
$sql = "SELECT * FROM $tbl_track_course WHERE user_id = $user_id";
if (isset ($cidReq) && !empty($cidReq)){
	$sql .= " AND course_code = $cidReq";
}
if (!isset ($last_date) || empty($last_date)){
	$last_date = date('Y-m-d');
}
if (!isset ($query_limit) || empty($query_limit)){
	$query_limit = 20;
}

$sql .= " AND login_course_date < '$last_date' ORDER BY login_course_date DESC LIMIT $query_limit";

//Optional: print out title to top of Excel or Word file with Timestamp
//for when file was generated:

//set $Use_Titel = 1 to generate title, 0 not to use title
$use_title = 1;

//define date for title: EDIT this to create the time-format you need
$now_date = date('m-d-Y H:i');

//define title for .doc or .xls file: EDIT this if you want
$title = "Dump For Table $tbl_track_course on $now_date";
/*
(Editing of code past this point recommended only for advanced users.)
*/
echo $sql;
//execute query
    $result = Database::query($sql)
	or die("Couldn't execute query:<br>" . mysql_error(). "<br>" . mysql_errno());

//if this parameter is included ($w=1), file returned will be in word format ('.doc')
//if parameter is not included, file returned will be in excel format ('.xls')
if (isset($w) && ($w==1))
{
	$file_type = "msword";
	$file_ending = "doc";
}else {
	$file_type = "vnd.ms-excel";
	$file_ending = "xls";
}
//header info for browser: determines file type ('.doc' or '.xls')
header("Content-Type: application/$file_type");
header("Content-Disposition: attachment; filename=database_dump.$file_ending");
header("Pragma: no-cache");
header("Expires: 0");

/*	Start of Formatting for Word or Excel	*/

if (isset($w) && ($w==1)) //check for $w again
{
	/*	FORMATTING FOR WORD DOCUMENTS ('.doc')   */
	//create title with timestamp:
	if ($use_title == 1)
	{
		echo("$title\n\n");
	}
	//define separator (defines columns in excel & tabs in word)
	$sep = "\n"; //new line character

	while($row = Database::fetch_row($result))
	{
		//set_time_limit(60); // HaRa
		$schema_insert = "";
		for($j=0; $j<mysql_num_fields($result);$j++)
		{
		//define field names
		$field_name = mysql_field_name($result,$j);
		//will show name of fields
		$schema_insert .= "$field_name:\t";
			if(!isset($row[$j])) {
				$schema_insert .= "NULL".$sep;
				}
			elseif ($row[$j] != "") {
				$schema_insert .= "$row[$j]".$sep;
				}
			else {
				$schema_insert .= "".$sep;
				}
		}
		$schema_insert = str_replace($sep."$", "", $schema_insert);
		$schema_insert .= "\t";
		print(trim($schema_insert));
		//end of each mysql row
		//creates line to separate data from each MySQL table row
		print "\n----------------------------------------------------\n";
	}
}else{
	/*	FORMATTING FOR EXCEL DOCUMENTS ('.xls')   */
	//create title with timestamp:
	if ($use_title == 1)
	{
		echo("$title\n");
	}
	//define separator (defines columns in excel & tabs in word)
	$sep = "\t"; //tabbed character

	//start of printing column names as names of MySQL fields
	for ($i = 0; $i < mysql_num_fields($result); $i++)
	{
		echo mysql_field_name($result,$i) . "\t";
	}
	print("\n");
	//end of printing column names

	//start while loop to get data
	while($row = Database::fetch_row($result))
	{
		//set_time_limit(60); // HaRa
		$schema_insert = "";
		for($j=0; $j<mysql_num_fields($result);$j++)
		{
			if(!isset($row[$j]))
				$schema_insert .= "NULL".$sep;
			elseif ($row[$j] != "")
				$schema_insert .= "$row[$j]".$sep;
			else
				$schema_insert .= "".$sep;
		}
		$schema_insert = str_replace($sep."$", "", $schema_insert);
		//following fix suggested by Josue (thanks, Josue!)
		//this corrects output in excel when table fields contain \n or \r
		//these two characters are now replaced with a space
		$schema_insert = preg_replace("/\r\n|\n\r|\n|\r/", " ", $schema_insert);
		$schema_insert .= "\t";
		print(trim($schema_insert));
		print "\n";
	}
}
/* FOOTER */
//Display :: display_footer();
?>