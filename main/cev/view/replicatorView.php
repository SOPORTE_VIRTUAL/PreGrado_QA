<?php
/**
 * @author Jorge Frisancho Jibaja <jrfdeft@gmail.com>, USIL - Some changes to allow mandatory surveys
 * @version CEV CHANGE 04FEB2015 cquispem@outlook.com compatibilidad a V1.10
 */
class ReplicateView{
    /**
     * @author Jorge Frisancho Jibaja <jrfdeft@gmail.com>, USIL - Some changes to allow mandatory surveys
     * @version CEV CHANGE 09FEB2012
     */
    public function __construct(){
        $language_file = array('admin','courses');
        $cidReset = true;
        require_once '../inc/global.inc.php';
        $this_section = SECTION_PLATFORM_ADMIN;

        api_protect_admin_script();
        require_once api_get_path(LIBRARY_PATH).'course.lib.php';
        require_once api_get_path(LIBRARY_PATH).'formvalidator/FormValidator.class.php';
        //CAMBIO DE sortabletable A NUEVA CLASE sortable_table para versión 1.10
        require_once api_get_path(LIBRARY_PATH).'sortable_table.class.php';

        $interbreadcrumb[] = array ("url" => '../../admin/index.php', "name" => get_lang('PlatformAdmin'));
        //$tool_name = get_lang('CourseList');
        $tool_name = 'Replicador de Cursos - Selecion de Curso MASTER';
        Display :: display_header($tool_name);
        if (isset($_GET['action'])) {
            switch ($_GET['action']) {
                case 'show_msg':
                    if (!empty($_GET['warn'])) {
                        Display::display_warning_message(urldecode($_GET['warn']));
                    }
                    if (!empty($_GET['msg'])) {
                        Display::display_normal_message(urldecode($_GET['msg']));
                    }
                    break;
                default:
                    break;
            }
        }

        $this->showView();

        Display :: display_footer();
    }

    /**
     * @author Jorge Frisancho Jibaja <jrfdeft@gmail.com>, USIL - Some changes to allow mandatory surveys
     * @version CEV CHANGE 09FEB2012
     */
    public function showDeaultView()
    {
        // Create a search-box
        $form = new FormValidator('search_simple','get','','','width=200px',false);
        $renderer =& $form->defaultRenderer();
        $renderer->setElementTemplate('<span>{element}</span> ');
        $form->addElement('text','keyword',get_lang('keyword'));
        $form->addElement('style_submit_button', 'submit', get_lang('SearchCourse'),'class="search"');
        $form->addElement('static','search_advanced_link',null,'<a href="../controller/replicatorController.php?search=advanced">'.get_lang('AdvancedSearch').'</a>');
        echo '<div style="float:right;margin-top:5px;margin-right:5px;">
                          <a href="'.api_get_path(WEB_CODE_PATH).'admin/course_add.php">'.Display::return_icon('course_add.gif',get_lang('AddCourse')).get_lang('AddCourse').'</a>
                 </div>';
        echo '<div class="actions">';
        $form->display();
        echo '</div>';
        // Create a sortable table with the course data
        $table = new SortableTable('courses', 'get_number_of_courses', 'get_course_data',2);
        $parameters=array();
        $table->set_additional_parameters($parameters);
        $table->set_header(0, '', false, 'width="8px"');
        $table->set_header(1, get_lang('Code'));
        $table->set_header(2, get_lang('Title'));
        $table->set_header(3, get_lang('Category'));
        $table->set_header(4, get_lang('Teacher'));
        $table->set_header(5, get_lang('Action'), false,'width="145px"');
        $table->set_column_filter(5,'modify_filter');
        $table->display();
    }
    
    /**
     * @author Jorge Frisancho Jibaja <jrfdeft@gmail.com>, USIL - Some changes to allow mandatory surveys
     * @version CEV CHANGE 09FEB2012
     */
    public function showCoursePicker($parameters)
    {
        // Create a search-box
        $form = new FormValidator('search_simple','get','','','width=200px',false);
        $renderer =& $form->defaultRenderer();
        $renderer->setElementTemplate('<span>{element}</span> ');
        $form->addElement('text','keyword',get_lang('keyword'));
        $form->addElement('style_submit_button', 'submit', get_lang('SearchCourse'),'class="search"');
        $form->addElement('static','search_advanced_link',null,'<a href="../controller/replicatorController.php?search=advanced">'.get_lang('AdvancedSearch').'</a>');
        echo '<div style="float:right;margin-top:5px;margin-right:5px;">
                          <a href="'.api_get_path(WEB_CODE_PATH).'admin/course_add.php">'.Display::return_icon('course_add.gif',get_lang('AddCourse')).get_lang('AddCourse').'</a>
                 </div>';
        echo '<div class="actions">';
        $form->display();
        echo '</div>';
        // Create a sortable table with the course data
        $table = new SortableTable('courses', 'get_number_of_courses', 'get_course_data',2);
        $table->set_additional_parameters($parameters);

        $table->set_header(0, '', false, 'width="8px"');
        $table->set_header(1, get_lang('Code'));
        $table->set_header(2, get_lang('Title'));
        $table->set_header(3, get_lang('Category'));
        $table->set_header(4, get_lang('Teacher'));
        $table->set_header(5, get_lang('Action'), false,'width="145px"');
        $table->set_column_filter(5,'modify_filter');
        $table->display();
    }
}

