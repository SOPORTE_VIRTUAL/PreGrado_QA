<?php
/**
 * @author Jorge Frisancho Jibaja <jrfdeft@gmail.com>, USIL - Some changes to allow mandatory surveys
 * @version CEV CHANGE 09FEB2012
 */
require_once dirname(__FILE__).'/../../cev/controller/querySurveyController.php';

function default_view(){
	$tool_name = 'Survey Query Executor';
	Display :: display_header($tool_name);
	get_search_form($tool_name);	
	Display :: display_footer();
}

function survey_list_view($_code, $_category){
	$tool_name = 'Survey Query Executor';
	Display :: display_header($tool_name);
	survey_list($_code, $_category);	
	Display :: display_footer();
}
