<?php

/**
 * PEAR and PEAR_Error classes, for error handling
 */
require_once 'PEAR.php';
/**
 * Base class for all HTML classes
 */
//require_once 'HTML/Common.php';

require_once 'HTML/QuickForm.php';

// define a new HTML_QuickForm object
$obj_registration_form = new HTML_QuickForm('frmRegistration', 'POST');

// add form elements, one-by-one
$obj_registration_form->addElement('header', '', 'Registration');
$obj_registration_form->addElement('html', '<tr><td colspan="2">&nbsp;</td></tr>');

$obj_registration_form->addElement('header', '', 'Artist Information');
$obj_registration_form->addElement('html', '<tr><td colspan="2">&nbsp;</td></tr>');

// Creates a group of text inputs
$obj_first_name = &HTML_QuickForm::createElement('text', '', '', array('size' => 30, 'maxlength' => 30));
$obj_last_name  = &HTML_QuickForm::createElement('text', '', '', array('size' => 30, 'maxlength' => 30));
$obj_registration_form->addGroup(array($obj_first_name, $obj_last_name), 'txtFullName', 'Full Name:', '&nbsp;');


$obj_registration_form->addElement('textarea', 'txtAddress', 'Address:', array('rows' => 3, 'cols' => 30));
$obj_registration_form->addElement('select', 'ddlCountry', 'Country:', array ("" => "Select Country", "USA" => "United States","UK" => "United Kingdom","IND" => "India", "Other" => "Other"));
$obj_registration_form->addElement('text', 'txtEmailAddress', 'Email Address:', array( 'size' => 40, 'maxlength' => 255));
$obj_registration_form->addElement('radio', 'radGender', 'Select Gender:', 'Male', 'M');
$obj_registration_form->addElement('radio', 'radGender', '', 'Female', 'F');
$obj_registration_form->addElement('date', 'txtDateOfBirth', 'Date of Birth:');

$obj_registration_form->addElement('html', '<tr><td colspan="2">&nbsp;</td></tr>');

$obj_registration_form->addElement('header', '', 'Group Information');

$obj_registration_form->addElement('html', '<tr><td colspan="2">&nbsp;</td></tr>');

$obj_registration_form->addElement('text', 'txtGroupName', 'Group Name:', array('size' => 40, 'maxlength' => 50));

// create a checkbox group
$obj_genre[] = &HTML_QuickForm::createElement('checkbox', 'Alter1native', null, 'Alternative444444');
$obj_genre[] = &HTML_QuickForm::createElement('checkbox', 'Hip-Hop', null, 'Hip-Hop');
$obj_genre[] = &HTML_QuickForm::createElement('checkbox', 'Other', null, 'Other');
$obj_registration_form->addGroup($obj_genre, 'chkGenre', 'Group Genre:', '<br />');

// create a radio button group
$obj_type[] = &HTML_QuickForm::createElement('radio', NULL, NULL, 'Male Solo', 'Male_Solo');
$obj_type[] = &HTML_QuickForm::createElement('radio', NULL, NULL, 'Female Solo', 'Female_Solo');
$obj_registration_form->addGroup($obj_type, 'radGroupType', 'Group Type:');

// create a multiple select drop down
$obj_registration_form->addElement('select', 'ddlLookingFor', 'Looking For:', array ("Publishing_Deal" => "Publishing Deal", "Label_Deal" => "Label Deal","Management_Deal" => "Management Deal", "Other" => "Other"), array("size" => "3", "multiple"));

$obj_registration_form->addElement('html', '<tr><td colspan="2">&nbsp;</td></tr>');
$obj_registration_form->addElement('header', '', 'Preferences');
$obj_registration_form->addElement('html', '<tr><td colspan="2">&nbsp;</td></tr>');

$obj_registration_form->addElement('checkbox', 'txtNewsletter', 'Subscribe to Newsletter:', 'Yes');
$obj_registration_form->addElement('html', '<tr><td colspan="2">&nbsp;</td></tr>');

// add a hidden value
$obj_registration_form->addElement('hidden', 'txtReferrer', 'http://www.mysite.com');

// creates a group of buttons to be displayed at the bottom of the form
$obj_submit[] = &HTML_QuickForm::createElement('submit', 'btnSubmit', 'Register');
$obj_submit[] = &HTML_QuickForm::createElement('reset', 'btnReset', 'Start Again');
$obj_registration_form->addGroup($obj_submit, '', '', '&nbsp;&nbsp');


// filters comes here

// validation rules come here

// validate form
if($obj_registration_form->validate()) {

	// filters after validation comes here

	// if data is valid, process form details
	echo '<pre>';
    var_dump($obj_registration_form->exportValues());
    echo '</pre>';

	// free the form values
	$obj_registration_form->freeze();

}

?>
<HTML>
<HEAD>
	<TITLE>Registration</TITLE>
	<BASEFONT face="Arial" size="2" >
</HEAD>
<BODY>
<?php

	// display the form
	$obj_registration_form->display();


?>
</BODY>
</HTML>

