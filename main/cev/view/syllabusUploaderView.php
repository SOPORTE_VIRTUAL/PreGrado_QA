<?php
/* Libraries */
require_once dirname(__FILE__).'/../../inc/global.inc.php';
$this_section = SECTION_COURSES;

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title> File Size - Course </title>
<link rel="stylesheet" type="text/css" href="../css/styles.css" />
<link rel="stylesheet" href="<?php echo api_get_path(WEB_LIBRARY_PATH);?>javascript/css/jquery-ui-1.8.7.custom.css" />
<script src="<?php echo api_get_path(WEB_LIBRARY_PATH);?>javascript/jquery-1.4.4.min.js" type="text/javascript" language="javascript"></script>
<script src="<?php echo api_get_path(WEB_LIBRARY_PATH);?>javascript/jquery-ui-1.8.7.custom.min.js" type="text/javascript" language="javascript"></script>

<script>
    var is_hidden = true;
    $(function() {      
        $( "#submenu" ).tabs();
    });
    function course_size(v_active, v_tmp) {
     if (v_active != 1){
         v_active = '';
     }
     if (v_tmp != 1) {
         v_tmp = '';
     }
     $( "#headerfile" ).empty();
     $( "#headersize" ).empty();
     $( "#subtitle" ).empty();
     $.post('queryController.php', {active: v_active, tmp: v_tmp}, function(query)
        {
            if (!query.is_empty){
                $("#listing").html(query.txt);
                $( "#headerfile" ).html(query.headerfile);
                $( "#headersize" ).html(query.headersize);
                $( "#subtitle" ).html(query.subtitle);
            }
            else{
                $("#listing").text("No existen registros para este rango");
            }

        }, "json");
        if (is_hidden){
            toggle('cev_results');
        }
    }

    function course_size_sort(v_active, v_tmp, v_name, v_order) {
     if (v_active != 1){
         v_active = '';
     }
     if (v_tmp != 1) {
         v_tmp = '';
     }
     if (v_name.is_empty){
         v_name = '';
     }
     if (v_order.is_empty){
         v_order = '';
     }

     $( "#headerfile" ).empty();
     $( "#headersize" ).empty();
     $.post('queryController.php', {active: v_active, tmp: v_tmp, name: v_name, order: v_order}, function(query)
        {
            if (!query.is_empty){
                $("#listing").html(query.txt);
                $( "#headerfile" ).html(query.headerfile);
                $( "#headersize" ).html(query.headersize);
            }
            else{
                $("#listing").text("No existen registros para este rango");
            }

        }, "json");
    }

    function upload_size_sort(v_name, v_order) {
     if (v_name.is_empty){
         v_name = '';
     }
     if (v_order.is_empty){
         v_order = '';
     }

     $( "#headerfile" ).empty();
     $( "#headersize" ).empty();
     $.post('uploadQueryController.php', {name: v_name, order: v_order}, function(query)
        {
            if (!query.is_empty){
                $("#listing").html(query.txt);
                $( "#headerfile" ).html(query.headerfile);
                $( "#headersize" ).html(query.headersize);
            }
            else{
                $("#listing").text("No existen registros para este rango");
            }

        }, "json");
    }

    function upload_size() {
     $( "#headerfile" ).empty();
     $( "#headersize" ).empty();
     $( "#subtitle" ).empty();
     $.post('uploadQueryController.php', {ok: "ok"}, function(query)
        {
            if (!query.is_empty){
                $("#listing").html(query.txt);
                $( "#headerfile" ).html(query.headerfile);
                $( "#headersize" ).html(query.headersize);
                $( "#subtitle" ).html(query.subtitle);
            }
            else{
                $("#listing").text("No existen registros para este rango");
            }

        }, "json");
        if (is_hidden){
            toggle('cev_results');
        }
    }
    function toggle(showHideDiv) {
	var ele = document.getElementById(showHideDiv);

	if(ele.style.display == "block") {
    		ele.style.display = "none";
                is_hidden = true;
  	}
	else {
		ele.style.display = "block";
                is_hidden = false;
	}
    }
</script>

</head>
<body>

<div id="container">
    <h1>CEV - Administration Queries</h1>
    <br />
    <div id="listingtitle">
        <h2><?php echo $title; ?></h2>
    </div>
    <div id="menu">
        <form enctype="multipart/form-data" action="../controller/syllabusUploaderController.php" method="post">
        <input type="hidden" name="MAX_FILE_SIZE" value="100000" />
        Ingrese el Directorio Origen:
        <input name="sourceDirectory" type="text" />
        <br />
        <input type="submit" value="Ejecutar" />
        </form>
    </div>

</div>
<div id="copy">CEV &copy;2010 Dev, <a href="http://twitter.com/#!/teocci">Jorge Frisancho</a>.</div>

</body>
</html>
