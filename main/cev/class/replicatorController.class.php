<?php
/**
 * @author Jorge Frisancho Jibaja <jrfdeft@gmail.com>, USIL - Some changes to allow mandatory surveys
 * @version CEV CHANGE 09FEB2012 cquispem@outlook.com compatibilidad a V1.10
 */
class replicatorController {
    /**
     * @author Jorge Frisancho Jibaja <jrfdeft@gmail.com>, USIL - Some changes to allow mandatory surveys
     * @version CEV CHANGE 09FEB2012
     */
    public function __construct(){
        $message = array();
        $language_file = array('admin','courses');
        $cidReset = true;
        require_once '../inc/global.inc.php';
        $this_section = SECTION_PLATFORM_ADMIN;

        api_protect_admin_script();
        require_once api_get_path(LIBRARY_PATH).'course.lib.php';
        require_once api_get_path(LIBRARY_PATH).'formvalidator/FormValidator.class.php';
        //CAMBIO DE sortabletable A NUEVA CLASE sortable_table para versión 1.10
        require_once api_get_path(LIBRARY_PATH).'sortable_table.class.php';

        if (isset($_GET['action'])) {
            switch ($_GET['action']) {
                case 'show_msg':
                    if (!empty($_GET['warn'])) {
                        $message[] = urldecode($_GET['warn']);
                    }
                    if (!empty($_GET['msg'])) {
                        $message[] = urldecode($_GET['msg']);
                    }
                    break;
                default:
                    break;
            }
        }
        
        $view = isset ($_GET['replicate_courses'])?$_GET['replicate_courses']:'';

        switch ($view) {
            case 'pick_courses':
                $this->showCoursePicker();
                break;
            case 'pick_courses':
                $this->showCoursePicker();
                break;
            default:
                $this->showDeaultView();
                break;
        }
        Display :: display_footer();
    }
    
    /**
     * @author Jorge Frisancho Jibaja <jrfdeft@gmail.com>, USIL - Some changes to allow mandatory surveys
     * @version CEV CHANGE 09FEB2012
     */    
    private function showDeaultView(){
        $myView = new vistaItems($lista);
        $this->mostarFormulario($this->_colItems);
    }
    
    /**
     * @author Jorge Frisancho Jibaja <jrfdeft@gmail.com>, USIL - Some changes to allow mandatory surveys
     * @version CEV CHANGE 09FEB2012
     */    
    public function showCoursePicker($lista) {
        $miVista = new vistaItems($lista);
    }

}
