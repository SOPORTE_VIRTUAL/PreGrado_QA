<?php
    require ('../inc/global.inc.php');
    $main_course_table 	= Database :: get_main_table(TABLE_MAIN_COURSE);
    $course_list       = array ();

    $course_list_sql = "SELECT course.directory as 'Directory'
                    FROM    ".$main_course_table."  course
                    WHERE visibility = 1 ORDER BY 1";

    $course_list_sql_result = Database::query($course_list_sql, __FILE__, __LINE__);



    while ($result_row = Database::fetch_array($course_list_sql_result))
    {
        $course_list[] = $result_row['Directory'];
    }

    $directory_list = array();
    $count = 0;
    $total = 0;

    foreach ($course_list as $key => $value) {
        $path                   = api_get_path(SYS_COURSE_PATH).$value;
        $fullpath               = $path."/temp";
        $size                   = recursive_directory_size($fullpath);
        $key                    = strtoupper($value);
        $directory_list[$key]   = $size;
        $total += $size;
        $count ++;
    }


    $baseurl = $_SERVER['PHP_SELF'] . '?';
    $fileurl = 'sort=name&amp;order=asc';
    $sizeurl = 'sort=size&amp;order=asc';
    //sort our files
    switch ($_GET['sort']) {
        case 'name':
            if($_GET['order']=='desc') {
                krsort($directory_list);
                $fileurl = 'sort=name&amp;order=asc';
                $sizeurl = 'sort=size&amp;order=asc';
            }
            else $fileurl = 'sort=name&amp;order=desc';
            break;
        case 'size':
            if($_GET['order']=='desc') {
                arsort($directory_list);
                $fileurl = 'sort=name&amp;order=asc';
                $sizeurl = 'sort=size&amp;order=asc';
            }
            else {
                asort($directory_list);
                $sizeurl = 'sort=size&amp;order=desc';
            }
            break;
        default:
            $fileurl = 'sort=name&amp;order=desc';
            $sizeurl = 'sort=size&amp;order=desc';
            break;
    }

    function filesize_r($path){
        if(!file_exists($path)) return 0;
        if(is_file($path)) return filesize($path);
        $ret = 0;
        foreach(glob($path."/*") as $fn)
        $ret += filesize_r($fn);
        return $ret;
    }

    function showSize($size_in_bytes) {
        $value = 0;
        if ($size_in_bytes >= 1073741824) {
            $value = round($size_in_bytes/1073741824*10)/10;
            return  ($round) ? round($value) . 'Gb' : "{$value} Gb";
        } else if ($size_in_bytes >= 1048576) {
            $value = round($size_in_bytes/1048576*10)/10;
            return  ($round) ? round($value) . 'Mb' : "{$value} Mb";
        } else if ($size_in_bytes >= 1024) {
            $value = round($size_in_bytes/1024*10)/10;
            return  ($round) ? round($value) . 'Kb' : "{$value} Kb";
        } else {
            return "{$size_in_bytes} Bytes";
        }
    }

        function recursive_directory_size($directory, $format=FALSE)
    {
            $size = 0;
            if(substr($directory,-1) == '/')
            {
                    $directory = substr($directory,0,-1);
            }
            if(!file_exists($directory) || !is_dir($directory) || !is_readable($directory))
            {
                    return -1;
            }
            if($handle = opendir($directory))
            {
                    while(($file = readdir($handle)) !== false)
                    {
                            $path = $directory.'/'.$file;
                            if($file != '.' && $file != '..')
                            {
                                    if(is_file($path))
                                    {
                                            $size += filesize($path);
                                    }elseif(is_dir($path))
                                    {
                                            $handlesize = recursive_directory_size($path);
                                            if($handlesize >= 0)
                                            {
                                                    $size += $handlesize;
                                            }else{
                                                    return -1;
                                            }
                                    }
                            }
                    }
                    closedir($handle);
            }
            if($format == TRUE)
            {
                    if($size / 1048576 > 1)
                    {
                            return round($size / 1048576, 1).' MB';
                    }elseif($size / 1024 > 1)
                    {
                            return round($size / 1024, 1).' KB';
                    }else{
                            return round($size, 1).' bytes';
                    }
            }else{
                    return $size;
            }
    }


    function getFileList($dir) {
        // array to hold return value
        $retval = array();
        // add trailing slash if missing
        if(substr($dir, -1) != "/") $dir .= "/";
        // open pointer to directory and read list of files
        $d = @dir($dir) or die("getFileList: Failed opening directory $dir for reading");
        while(false !== ($entry = $d->read())) {
            // skip hidden files if($entry[0] == ".") continue;
            if(is_dir("$dir$entry")){
                $retval[] = array( "name" => "$dir$entry/", "type" => filetype("$dir$entry"), "size" => 0, "lastmod" => filemtime("$dir$entry") );
            }
            elseif(is_readable("$dir$entry")) {
                $retval[] = array( "name" => "$dir$entry", "type" => mime_content_type("$dir$entry"), "size" => filesize("$dir$entry"), "lastmod" => filemtime("$dir$entry") );
            }
        }
        $d->close();
        return $retval;
    }


    function dir_size( $dir )
    {
    if( !$dir or !is_dir( $dir ) )
    {
        return 0;
    }

    $ret = 0;
    $sub = opendir( $dir );
    while( $file = readdir( $sub ) )
    {
        if( is_dir( $dir . '/' . $file ) && $file !== ".." && $file !== "." )
        {
            $ret += dir_size( $dir . '/' . $file );
            unset( $file );
        }
        elseif( !is_dir( $dir . '/' . $file ) )
        {
            $stats = stat( $dir . '/' . $file );
            $ret += $stats['size'];
            unset( $file );
        }
    }
    closedir( $sub );
    unset( $sub );
    return $ret;
    }
?>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title> File Size - Course/Temp </title>
<link rel="stylesheet" type="text/css" href="dlf/styles.css" />
</head>
<body>

<div id="container">
    <h1>CEV - Administration Queries</h1>
    <br />
    <h2>Tama&ntilde;o de los directorios "TEMP" | Total Size: <?php echo showSize($total) ?></h2>
    <div id="listingcontainer">
        <div id="listingheader">
            <div id="headerfile"><a href="<?php echo $baseurl . $fileurl;?>">Directorio</a></div>
            <div id="headersize"><a href="<?php echo $baseurl . $sizeurl;?>">Tama&ntilde;o</a></div>
            <div id="headermodified">-----</div>
        </div>
        <div id="listing">
            <?php
            $class = 'b';
            foreach ($directory_list as $key => $val) {
                echo '<div class="'.$class.'"><img alt="'.$key.'" src="dlf/folder.png"><strong>'.$key.'/temp</strong> <em>'.showSize($val).'</em>'.date ("M d Y h:i:s A").'</div>';
                if($class=='b') $class='w';
		else $class = 'b';
            }
            ?>
        </div>
    </div>
</div>

</body>
</html>