<?php
    require ('../../inc/global.inc.php');

    require_once ('../../cev/model/sizer.lib.php');
    
    $main_course_table      = Database :: get_main_table(TABLE_MAIN_COURSE);
    $course_list            = array ();

    $course_list_sql        = "SELECT course.directory as 'Directory'
                    FROM    ".$main_course_table."  course
                    WHERE visibility = 1 ORDER BY 1";

    $course_list_sql_result = Database::query($course_list_sql, __FILE__, __LINE__);

    while ($result_row      = Database::fetch_array($course_list_sql_result))
    {
        $course_list[]      = $result_row['Directory'];
    }

    $directory_list         = array();
    $count                  = 0;
    $total                  = 0;
    $foo_dir                = array();

    foreach ($course_list as $key => $value) {
        $path                   = api_get_path(SYS_COURSE_PATH).$value;
        $fullpath               = $path;
        $foo_dir[]              = $fullpath;
        $size                   = recursive_directory_size($fullpath);
        //$size                   = filesize_r($fullpath);
        $key                    = strtoupper($value);
        $directory_list[$key]   = $size;
        $total += $size;
        $count ++;
    }


    $baseurl = $_SERVER['PHP_SELF'] . '?';
    $fileurl = 'sort=name&amp;order=asc';
    $sizeurl = 'sort=size&amp;order=asc';
    //sort our files
    switch ($_GET['sort']) {
        case 'name':
            if($_GET['order']=='desc') {
                krsort($directory_list);
                $fileurl = 'sort=name&amp;order=asc';
                $sizeurl = 'sort=size&amp;order=asc';
            }
            else $fileurl = 'sort=name&amp;order=desc';
            break;
        case 'size':
            if($_GET['order']=='desc') {
                arsort($directory_list);
                $fileurl = 'sort=name&amp;order=asc';
                $sizeurl = 'sort=size&amp;order=asc';
            }
            else {
                asort($directory_list);
                $sizeurl = 'sort=size&amp;order=desc';
            }
            break;
        default:
            $fileurl = 'sort=name&amp;order=desc';
            $sizeurl = 'sort=size&amp;order=desc';
            break;
    }

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title> File Size - Course </title>
<link rel="stylesheet" type="text/css" href="../dlf/styles.css" />
<link rel="stylesheet" href="<?php echo api_get_path(WEB_LIBRARY_PATH);?>javascript/css/jquery-ui-1.8.7.custom.css" />
<script src="<?php echo api_get_path(WEB_LIBRARY_PATH);?>javascript/jquery-1.4.4.min.js" type="text/javascript" language="javascript"></script>
<script src="<?php echo api_get_path(WEB_LIBRARY_PATH);?>javascript/jquery-ui-1.8.7.custom.min.js" type="text/javascript" language="javascript"></script>

</head>
<body>

<div id="container2">
    <h1>CEV - Administration Queries</h1>
    <br />
    <div id="listingtitle">
        <h2>Tama&ntilde;o de los directorios de cada CURSO | Total Size: <?php echo showSize($total) ?></h2>
    </div>
    <div id="listingcontainer">
        <div id="listingheader">
            <div id="headerfile"><a href="<?php echo $baseurl . $fileurl;?>">Directorio</a></div>
            <div id="headersize"><a href="<?php echo $baseurl . $sizeurl;?>">Tama&ntilde;o</a></div>
            <div id="headermodified">-----</div>
        </div>
        <div id="listing">
            <?php
            $class = 'b';
            foreach ($directory_list as $key => $val) {
                echo '<div class="'.$class.'"><img alt="'.$key.'" src="../dlf/folder.png"><strong>'.$key.'</strong> <em>'.showSize($val).'</em>'.date ("M d Y h:i:s A").'</div>';
                if($class=='b') $class='w';
		else $class = 'b';
            }

            ?>

        </div>
        <?php

            foreach ($foo_dir as $k =>$item) {
                $dir = getFileList($item);
                foreach ($dir as $val) {
                   print_r($val);
                   echo "<br />";
                }
            }
        ?>
    </div>
</div>
<div id="copy">CEV &copy;2010 Dev, <a href="http://twitter.com/#!/teocci">Jorge Frisancho</a>.</div>

</body>
</html>